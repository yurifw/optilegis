drop database legaltracker;
create database if not exists legaltracker;
use legaltracker;

create table if not exists escritorio(
	id int auto_increment primary key,
	nome varchar(100) not null,
	subscription_id varchar(30),
	notificar_falta_pagamento boolean,
	data_cadastro datetime
);

create table if not exists usuario(
	id int auto_increment primary key,
	nome varchar(100),
	email varchar(100) not null unique,
	senha varchar(128),
	escritorio_id int not null,
	admin boolean not null,
	foreign key (escritorio_id) references escritorio(id)
);

create table convite(
	id int auto_increment primary key,
	codigo varchar(20) not null,
	email varchar(100) not null unique,
	convidado_por int not null,
	foreign key (convidado_por) references usuario(id)
);

create table if not exists termo(
	id int auto_increment primary key,
	termo varchar(100) not null,
	escritorio_id int not null,
	unique(termo, escritorio_id),
	foreign key (escritorio_id) references escritorio(id)
);
ALTER TABLE termo CONVERT TO CHARACTER SET utf8mb4 collate utf8mb4_bin;

create table if not exists usuario_termos(
	id int auto_increment primary key,
	usuario_id int not null,
	termo_id int not null,
	unique(usuario_id, termo_id),
	foreign key (usuario_id) references usuario(id) on delete cascade,
	foreign key (termo_id) references termo(id) on delete cascade
);

create table diario_oficial(
	id int auto_increment primary key,
	data_publicacao date null,
	arquivo longblob not null
);

create table pauta(
	id int auto_increment primary key,
	turma varchar(4),
	num_processo varchar(30),
	autuada varchar(200),
	responsavel varchar(100),
	relator varchar(200),
	data datetime,
	sessao_link varchar(300),
	sessao_num varchar(30),
	sessao_senha varchar(30),
	sessao_status varchar(30),
	unique(num_processo, sessao_num)
);


insert into escritorio (nome, data_cadastro,notificar_falta_pagamento) values ("Escritorio Teste", now(), 1);
insert into escritorio (nome, data_cadastro,notificar_falta_pagamento) values ("Moacyr Oliveira Advogados", now(), 1);
insert into usuario (nome, email, escritorio_id, admin, senha) values ('Yuri', 'yuridefw2@gmail.com', 1, 1, '$2b$12$G/7WvMFWVeP/vHjdOtHa..wn81jeoxRkIIJQA8UspOijeBDUyJrle');  -- senha 123456
insert into usuario (nome, email, escritorio_id, admin, senha) values ('Thiago Conhasca', 'thiagoconhasca@moacyroliveira.com.br', 2, 1, '$2b$12$G/7WvMFWVeP/vHjdOtHa..wn81jeoxRkIIJQA8UspOijeBDUyJrle');  -- senha 123456


SET old_passwords=0;
create user IF NOT EXISTS 'legaluser'@'localhost' identified by 'zQqB0nrIDokxEcS14yTD';
GRANT ALL PRIVILEGES ON legaltracker . * TO 'legaluser'@'localhost';
FLUSH PRIVILEGES;
