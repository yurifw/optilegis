function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./src/app/api.interceptor.ts":
  /*!************************************!*\
    !*** ./src/app/api.interceptor.ts ***!
    \************************************/

  /*! exports provided: ApiInterceptor */

  /***/
  function srcAppApiInterceptorTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ApiInterceptor", function () {
      return ApiInterceptor;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ngx-toastr */
    "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");

    var ApiInterceptor =
    /*#__PURE__*/
    function () {
      function ApiInterceptor(toastr) {
        _classCallCheck(this, ApiInterceptor);

        this.toastr = toastr;
      }

      _createClass(ApiInterceptor, [{
        key: "intercept",
        value: function intercept(req, next) {
          var _this = this;

          var newHeaders = {
            'Authorization': localStorage.getItem('jwt') || ""
          };
          var cloneReq = req.clone({
            setHeaders: newHeaders
          });
          return next.handle(cloneReq).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(function (event) {
            //toasts message from server
            if (event.body && event.body.msg && event.body.success != undefined) {
              if (event.body.success) {
                _this.toastr.success(event.body.msg);
              } else {
                _this.toastr.error(event.body.msg);
              }
            }
          }));
        }
      }]);

      return ApiInterceptor;
    }();

    ApiInterceptor.ɵfac = function ApiInterceptor_Factory(t) {
      return new (t || ApiInterceptor)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]));
    };

    ApiInterceptor.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: ApiInterceptor,
      factory: ApiInterceptor.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ApiInterceptor, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/app-routing.module.ts":
  /*!***************************************!*\
    !*** ./src/app/app-routing.module.ts ***!
    \***************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _components_tela_login_tela_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./components/tela-login/tela-login.component */
    "./src/app/components/tela-login/tela-login.component.ts");
    /* harmony import */


    var _components_tela_termos_tela_termos_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./components/tela-termos/tela-termos.component */
    "./src/app/components/tela-termos/tela-termos.component.ts");
    /* harmony import */


    var _components_tela_usuarios_tela_usuarios_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./components/tela-usuarios/tela-usuarios.component */
    "./src/app/components/tela-usuarios/tela-usuarios.component.ts");
    /* harmony import */


    var _components_tela_convite_tela_convite_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./components/tela-convite/tela-convite.component */
    "./src/app/components/tela-convite/tela-convite.component.ts");
    /* harmony import */


    var _components_tela_config_tela_config_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./components/tela-config/tela-config.component */
    "./src/app/components/tela-config/tela-config.component.ts");
    /* harmony import */


    var _components_tela_pagamento_tela_pagamento_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./components/tela-pagamento/tela-pagamento.component */
    "./src/app/components/tela-pagamento/tela-pagamento.component.ts");

    var routes = [{
      path: 'termos',
      component: _components_tela_termos_tela_termos_component__WEBPACK_IMPORTED_MODULE_3__["TelaTermosComponent"]
    }, {
      path: 'usuarios',
      component: _components_tela_usuarios_tela_usuarios_component__WEBPACK_IMPORTED_MODULE_4__["TelaUsuariosComponent"]
    }, {
      path: '',
      component: _components_tela_login_tela_login_component__WEBPACK_IMPORTED_MODULE_2__["TelaLoginComponent"]
    }, {
      path: 'login',
      component: _components_tela_login_tela_login_component__WEBPACK_IMPORTED_MODULE_2__["TelaLoginComponent"]
    }, {
      path: 'login/:jwt',
      component: _components_tela_login_tela_login_component__WEBPACK_IMPORTED_MODULE_2__["TelaLoginComponent"]
    }, {
      path: 'convite/:codigo',
      component: _components_tela_convite_tela_convite_component__WEBPACK_IMPORTED_MODULE_5__["TelaConviteComponent"]
    }, {
      path: 'config',
      component: _components_tela_config_tela_config_component__WEBPACK_IMPORTED_MODULE_6__["TelaConfigComponent"]
    }, {
      path: 'pagamento',
      component: _components_tela_pagamento_tela_pagamento_component__WEBPACK_IMPORTED_MODULE_7__["TelaPagamentoComponent"]
    }];

    var AppRoutingModule = function AppRoutingModule() {
      _classCallCheck(this, AppRoutingModule);
    };

    AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: AppRoutingModule
    });
    AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function AppRoutingModule_Factory(t) {
        return new (t || AppRoutingModule)();
      },
      imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, {
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function AppComponent_div_0_Template(rf, ctx) {
      if (rf & 1) {
        var _r40 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_div_0_Template_button_click_6_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r40);

          var ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r39.logout();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " Sair ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "i", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r36.nome);
      }
    }

    var _c0 = function _c0(a0) {
      return {
        "col-2": a0,
        "d-block": true,
        "d-sm-none": true
      };
    };

    function AppComponent_div_1_div_6_Template(rf, ctx) {
      if (rf & 1) {
        var _r44 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_div_1_div_6_Template_div_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r44);

          var ctx_r43 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r43.navigate("/usuarios");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](1, _c0, ctx_r41.isAdmin));
      }
    }

    function AppComponent_div_1_div_7_Template(rf, ctx) {
      if (rf & 1) {
        var _r46 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_div_1_div_7_Template_div_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46);

          var ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r45.navigate("/pagamento");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r42 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](1, _c0, ctx_r42.isAdmin));
      }
    }

    var _c1 = function _c1(a0) {
      return {
        "col-1": a0
      };
    };

    var _c2 = function _c2(a0, a1) {
      return {
        "col-2": a0,
        "col-4": a1,
        "d-block": true,
        "d-sm-none": true
      };
    };

    function AppComponent_div_1_Template(rf, ctx) {
      if (rf & 1) {
        var _r48 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_div_1_Template_div_click_3_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r48);

          var ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r47.navigate("/termos");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "i", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, AppComponent_div_1_div_6_Template, 3, 3, "div", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, AppComponent_div_1_div_7_Template, 3, 3, "div", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_div_1_Template_div_click_9_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r48);

          var ctx_r49 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r49.navigate("/config");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "i", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_div_1_Template_div_click_12_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r48);

          var ctx_r50 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r50.logout();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "i", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "div", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](7, _c1, ctx_r37.isAdmin));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](9, _c2, ctx_r37.isAdmin, !ctx_r37.isAdmin));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r37.isAdmin);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r37.isAdmin);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](12, _c2, ctx_r37.isAdmin, !ctx_r37.isAdmin));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](15, _c2, ctx_r37.isAdmin, !ctx_r37.isAdmin));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](18, _c1, ctx_r37.isAdmin));
      }
    }

    function AppComponent_div_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r52 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_div_3_Template_div_click_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r52);

          var ctx_r51 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r51.navigate("/termos");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, " Gerenciar Termos");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_div_3_Template_div_click_5_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r52);

          var ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r53.navigate("/usuarios");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "i", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " Usu\xE1rios");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_div_3_Template_div_click_8_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r52);

          var ctx_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r54.navigate("/pagamento");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "i", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " Pagamento");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_div_3_Template_div_click_11_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r52);

          var ctx_r55 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r55.navigate("/config");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "i", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, " Configura\xE7\xF5es");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", !ctx_r38.isAdmin);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", !ctx_r38.isAdmin);
      }
    }

    var _c3 = function _c3(a0, a1, a2, a3) {
      return {
        "col-sm-8": a0,
        "col-md-9": a1,
        "col-lg-10": a2,
        "col-12": a3
      };
    };

    var AppComponent =
    /*#__PURE__*/
    function () {
      function AppComponent(auth, router) {
        _classCallCheck(this, AppComponent);

        this.auth = auth;
        this.router = router;
        this.title = 'LegalTracker';
        this.isLoggedIn = false;
        this.isAdmin = false;
        this.nome = "";
      }

      _createClass(AppComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this2 = this;

          this.auth.loggedIn.subscribe(function (isLoggedIn) {
            _this2.isLoggedIn = isLoggedIn;

            if (isLoggedIn) {
              _this2.updateUser();
            }
          });
          this.updateUser();
        }
      }, {
        key: "updateUser",
        value: function updateUser() {
          var _this3 = this;

          this.auth.verifyToken().subscribe(function (resp) {
            _this3.isLoggedIn = resp.data.isValid;
            _this3.nome = resp.data.payload.nome;
            _this3.isAdmin = resp.data.payload.admin;
          });
        }
      }, {
        key: "logout",
        value: function logout() {
          localStorage.removeItem('jwt');
          this.isLoggedIn = false;
          this.auth.loggedIn.emit(false);
          this.router.navigate(['']);
        }
      }, {
        key: "navigate",
        value: function navigate(route) {
          this.router.navigate([route]);
        }
      }]);

      return AppComponent;
    }();

    AppComponent.ɵfac = function AppComponent_Factory(t) {
      return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]));
    };

    AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: AppComponent,
      selectors: [["app-root"]],
      decls: 6,
      vars: 9,
      consts: [["class", "row ", "style", "padding-left:15px; padding-right:15px;", 4, "ngIf"], ["class", "row", "style", "padding-left:15px; padding-right:15px;", 4, "ngIf"], [1, "row", "menu-row", 2, "padding-left", "15px", "padding-right", "15px"], ["class", "col-sm-4 col-md-3 col-lg-2 d-none d-sm-block", 4, "ngIf"], [3, "ngClass"], [1, "row", 2, "padding-left", "15px", "padding-right", "15px"], [1, "col-12", "d-none", "d-sm-block"], [1, "header-row"], [1, "nome"], [1, "btn-logout"], ["type", "button", 1, "btn", "btn-primary", 3, "click"], [1, "fas", "fa-sign-out-alt"], [2, "padding-left", "7px", "padding-right", "7px", 3, "ngClass"], [1, "menu-mobile", "text-center", 3, "click"], [2, "margin", "auto"], [1, "fas", "fa-file-contract"], ["style", "padding-left:7px;padding-right:7px;", 3, "ngClass", 4, "ngIf"], [1, "fas", "fa-cog"], [1, "fas", "fa-users"], [1, "far", "fa-money-bill-alt"], [1, "col-sm-4", "col-md-3", "col-lg-2", "d-none", "d-sm-block"], [1, "menu"], [1, "menu-item", "top", 3, "click"], [1, "menu-item", 3, "hidden", "click"], [1, "menu-item", 3, "click"]],
      template: function AppComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, AppComponent_div_0_Template, 9, 1, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, AppComponent_div_1_Template, 15, 20, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, AppComponent_div_3_Template, 14, 2, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "router-outlet");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLoggedIn);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLoggedIn);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLoggedIn);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction4"](4, _c3, ctx.isLoggedIn, ctx.isLoggedIn, ctx.isLoggedIn, !ctx.isLoggedIn));
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgClass"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"]],
      styles: [".header-row[_ngcontent-%COMP%]{\n\theight: 60px;\n\tbackground-color: #ffffff;\n\tborder-radius: 15px;\n\twidth: 100%;\n\tmargin-bottom: 15px;\n\tmargin-top: 15px;\n\t\n\tpadding-left: 15px;\n\tpadding-right: 15px;\n\topacity: 0.8;\n\tdisplay: flex;\n\talign-items: center;\n}\n\n.menu-row[_ngcontent-%COMP%]{\n\theight: calc(90% - 90px);\n\topacity: 0.8;\n}\n\n.nome[_ngcontent-%COMP%]{\n\tfont-weight: bolder;\n\tfont-size: 110%;\n}\n\n.btn-logout[_ngcontent-%COMP%]{\n\tmargin-left: auto;\n}\n\n.menu[_ngcontent-%COMP%]{\n\tbackground-color: #ffffff;\n\tborder-radius: 15px;\n\twidth: 100%;\n\theight: 100%;\n\tpadding-top: 7px;\n}\n\n.menu-mobile[_ngcontent-%COMP%]{\n\tmargin-bottom: 15px;\n\tmargin-top: 15px;\n\theight: 60px;\n\tbackground-color: #ffffff;\n\tborder-radius: 15px;\n\twidth: 100%;\n\topacity: 0.8;\n\talign-items: center;\n\tfont-size: 35px;\n}\n\n.top[_ngcontent-%COMP%] {\n\tborder-top-left-radius: 15px;\n\tborder-top-right-radius: 15px;\n}\n\n.menu-item[_ngcontent-%COMP%]{\n\tborder-style: none;\n\tpadding-left: 15px;\n\tpadding-bottom: 7px;\n\tpadding-top: 7px;\n\tborder-color: #c3c7c7;\n}\n\n.menu-item[_ngcontent-%COMP%]:hover{\n\tcursor: pointer;\n\ttext-decoration: underline;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Q0FDQyxZQUFZO0NBQ1oseUJBQXlCO0NBQ3pCLG1CQUFtQjtDQUNuQixXQUFXO0NBQ1gsbUJBQW1CO0NBQ25CLGdCQUFnQjtDQUNoQjtxQkFDb0I7Q0FDcEIsa0JBQWtCO0NBQ2xCLG1CQUFtQjtDQUNuQixZQUFZO0NBQ1osYUFBYTtDQUNiLG1CQUFtQjtBQUNwQjs7QUFFQTtDQUNDLHdCQUF3QjtDQUN4QixZQUFZO0FBQ2I7O0FBQ0E7Q0FDQyxtQkFBbUI7Q0FDbkIsZUFBZTtBQUNoQjs7QUFDQTtDQUNDLGlCQUFpQjtBQUNsQjs7QUFFQTtDQUNDLHlCQUF5QjtDQUN6QixtQkFBbUI7Q0FDbkIsV0FBVztDQUNYLFlBQVk7Q0FDWixnQkFBZ0I7QUFDakI7O0FBRUE7Q0FDQyxtQkFBbUI7Q0FDbkIsZ0JBQWdCO0NBQ2hCLFlBQVk7Q0FDWix5QkFBeUI7Q0FDekIsbUJBQW1CO0NBQ25CLFdBQVc7Q0FDWCxZQUFZO0NBQ1osbUJBQW1CO0NBQ25CLGVBQWU7QUFDaEI7O0FBR0E7Q0FDQyw0QkFBNEI7Q0FDNUIsNkJBQTZCO0FBQzlCOztBQUVBO0NBQ0Msa0JBQWtCO0NBQ2xCLGtCQUFrQjtDQUNsQixtQkFBbUI7Q0FDbkIsZ0JBQWdCO0NBQ2hCLHFCQUFxQjtBQUN0Qjs7QUFFQTtDQUNDLGVBQWU7Q0FDZiwwQkFBMEI7QUFDM0IiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXItcm93e1xuXHRoZWlnaHQ6IDYwcHg7XG5cdGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG5cdGJvcmRlci1yYWRpdXM6IDE1cHg7XG5cdHdpZHRoOiAxMDAlO1xuXHRtYXJnaW4tYm90dG9tOiAxNXB4O1xuXHRtYXJnaW4tdG9wOiAxNXB4O1xuXHQvKiBtYXJnaW4tbGVmdDoxNXB4O1xuXHRtYXJnaW4tcmlnaHQ6MTVweDsgKi9cblx0cGFkZGluZy1sZWZ0OiAxNXB4O1xuXHRwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuXHRvcGFjaXR5OiAwLjg7XG5cdGRpc3BsYXk6IGZsZXg7XG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5tZW51LXJvd3tcblx0aGVpZ2h0OiBjYWxjKDkwJSAtIDkwcHgpO1xuXHRvcGFjaXR5OiAwLjg7XG59XG4ubm9tZXtcblx0Zm9udC13ZWlnaHQ6IGJvbGRlcjtcblx0Zm9udC1zaXplOiAxMTAlO1xufVxuLmJ0bi1sb2dvdXR7XG5cdG1hcmdpbi1sZWZ0OiBhdXRvO1xufVxuXG4ubWVudXtcblx0YmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcblx0Ym9yZGVyLXJhZGl1czogMTVweDtcblx0d2lkdGg6IDEwMCU7XG5cdGhlaWdodDogMTAwJTtcblx0cGFkZGluZy10b3A6IDdweDtcbn1cblxuLm1lbnUtbW9iaWxle1xuXHRtYXJnaW4tYm90dG9tOiAxNXB4O1xuXHRtYXJnaW4tdG9wOiAxNXB4O1xuXHRoZWlnaHQ6IDYwcHg7XG5cdGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG5cdGJvcmRlci1yYWRpdXM6IDE1cHg7XG5cdHdpZHRoOiAxMDAlO1xuXHRvcGFjaXR5OiAwLjg7XG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cdGZvbnQtc2l6ZTogMzVweDtcbn1cblxuXG4udG9wIHtcblx0Ym9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMTVweDtcblx0Ym9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDE1cHg7XG59XG5cbi5tZW51LWl0ZW17XG5cdGJvcmRlci1zdHlsZTogbm9uZTtcblx0cGFkZGluZy1sZWZ0OiAxNXB4O1xuXHRwYWRkaW5nLWJvdHRvbTogN3B4O1xuXHRwYWRkaW5nLXRvcDogN3B4O1xuXHRib3JkZXItY29sb3I6ICNjM2M3Yzc7XG59XG5cbi5tZW51LWl0ZW06aG92ZXJ7XG5cdGN1cnNvcjogcG9pbnRlcjtcblx0dGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-root',
          templateUrl: './app.component.html',
          styleUrls: ['./app.component.css']
        }]
      }], function () {
        return [{
          type: _services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/platform-browser/animations */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/animations.js");
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-toastr */
    "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
    /* harmony import */


    var _pipes_credit_card_pipe__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./pipes/credit-card.pipe */
    "./src/app/pipes/credit-card.pipe.ts");
    /* harmony import */


    var _pipes_cpf_cnpj_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./pipes/cpf-cnpj.pipe */
    "./src/app/pipes/cpf-cnpj.pipe.ts");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./app-routing.module */
    "./src/app/app-routing.module.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _components_tela_login_tela_login_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./components/tela-login/tela-login.component */
    "./src/app/components/tela-login/tela-login.component.ts");
    /* harmony import */


    var _api_interceptor__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./api.interceptor */
    "./src/app/api.interceptor.ts");
    /* harmony import */


    var _components_tela_termos_tela_termos_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./components/tela-termos/tela-termos.component */
    "./src/app/components/tela-termos/tela-termos.component.ts");
    /* harmony import */


    var _components_termos_manager_admin_termos_manager_admin_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./components/termos-manager-admin/termos-manager-admin.component */
    "./src/app/components/termos-manager-admin/termos-manager-admin.component.ts");
    /* harmony import */


    var _components_termos_manager_user_termos_manager_user_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./components/termos-manager-user/termos-manager-user.component */
    "./src/app/components/termos-manager-user/termos-manager-user.component.ts");
    /* harmony import */


    var _components_tela_usuarios_tela_usuarios_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ./components/tela-usuarios/tela-usuarios.component */
    "./src/app/components/tela-usuarios/tela-usuarios.component.ts");
    /* harmony import */


    var _components_tela_convite_tela_convite_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ./components/tela-convite/tela-convite.component */
    "./src/app/components/tela-convite/tela-convite.component.ts");
    /* harmony import */


    var _components_tela_config_tela_config_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ./components/tela-config/tela-config.component */
    "./src/app/components/tela-config/tela-config.component.ts");
    /* harmony import */


    var _components_tela_pagamento_tela_pagamento_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ./components/tela-pagamento/tela-pagamento.component */
    "./src/app/components/tela-pagamento/tela-pagamento.component.ts");

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
      type: AppModule,
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
    });
    AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
      factory: function AppModule_Factory(t) {
        return new (t || AppModule)();
      },
      providers: [{
        provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"],
        useClass: _api_interceptor__WEBPACK_IMPORTED_MODULE_11__["ApiInterceptor"],
        multi: true
      }],
      imports: [[_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrModule"].forRoot({
        positionClass: 'toast-bottom-center',
        preventDuplicates: true
      })]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](AppModule, {
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"], _components_tela_login_tela_login_component__WEBPACK_IMPORTED_MODULE_10__["TelaLoginComponent"], _components_tela_termos_tela_termos_component__WEBPACK_IMPORTED_MODULE_12__["TelaTermosComponent"], _components_termos_manager_admin_termos_manager_admin_component__WEBPACK_IMPORTED_MODULE_13__["TermosManagerAdminComponent"], _components_termos_manager_user_termos_manager_user_component__WEBPACK_IMPORTED_MODULE_14__["TermosManagerUserComponent"], _components_tela_usuarios_tela_usuarios_component__WEBPACK_IMPORTED_MODULE_15__["TelaUsuariosComponent"], _components_tela_convite_tela_convite_component__WEBPACK_IMPORTED_MODULE_16__["TelaConviteComponent"], _components_tela_config_tela_config_component__WEBPACK_IMPORTED_MODULE_17__["TelaConfigComponent"], _components_tela_pagamento_tela_pagamento_component__WEBPACK_IMPORTED_MODULE_18__["TelaPagamentoComponent"], _pipes_credit_card_pipe__WEBPACK_IMPORTED_MODULE_6__["CreditCardNumberPipe"], _pipes_credit_card_pipe__WEBPACK_IMPORTED_MODULE_6__["CreditCardExpirationPipe"], _pipes_cpf_cnpj_pipe__WEBPACK_IMPORTED_MODULE_7__["CpfCnpjPipe"]],
        imports: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"],
        args: [{
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"], _components_tela_login_tela_login_component__WEBPACK_IMPORTED_MODULE_10__["TelaLoginComponent"], _components_tela_termos_tela_termos_component__WEBPACK_IMPORTED_MODULE_12__["TelaTermosComponent"], _components_termos_manager_admin_termos_manager_admin_component__WEBPACK_IMPORTED_MODULE_13__["TermosManagerAdminComponent"], _components_termos_manager_user_termos_manager_user_component__WEBPACK_IMPORTED_MODULE_14__["TermosManagerUserComponent"], _components_tela_usuarios_tela_usuarios_component__WEBPACK_IMPORTED_MODULE_15__["TelaUsuariosComponent"], _components_tela_convite_tela_convite_component__WEBPACK_IMPORTED_MODULE_16__["TelaConviteComponent"], _components_tela_config_tela_config_component__WEBPACK_IMPORTED_MODULE_17__["TelaConfigComponent"], _components_tela_pagamento_tela_pagamento_component__WEBPACK_IMPORTED_MODULE_18__["TelaPagamentoComponent"], _pipes_credit_card_pipe__WEBPACK_IMPORTED_MODULE_6__["CreditCardNumberPipe"], _pipes_credit_card_pipe__WEBPACK_IMPORTED_MODULE_6__["CreditCardExpirationPipe"], _pipes_cpf_cnpj_pipe__WEBPACK_IMPORTED_MODULE_7__["CpfCnpjPipe"]],
          imports: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrModule"].forRoot({
            positionClass: 'toast-bottom-center',
            preventDuplicates: true
          })],
          providers: [{
            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"],
            useClass: _api_interceptor__WEBPACK_IMPORTED_MODULE_11__["ApiInterceptor"],
            multi: true
          }],
          bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/commons.ts":
  /*!****************************!*\
    !*** ./src/app/commons.ts ***!
    \****************************/

  /*! exports provided: SERVER */

  /***/
  function srcAppCommonsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SERVER", function () {
      return SERVER;
    });

    var SERVER = "https://admdo.com.br/api";
    /***/
  },

  /***/
  "./src/app/components/tela-config/tela-config.component.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/components/tela-config/tela-config.component.ts ***!
    \*****************************************************************/

  /*! exports provided: TelaConfigComponent */

  /***/
  function srcAppComponentsTelaConfigTelaConfigComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TelaConfigComponent", function () {
      return TelaConfigComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ngx-toastr */
    "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
    /* harmony import */


    var _services_pagamento_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/pagamento.service */
    "./src/app/services/pagamento.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

    var TelaConfigComponent =
    /*#__PURE__*/
    function () {
      function TelaConfigComponent(authService, toastr, pagamentoService) {
        _classCallCheck(this, TelaConfigComponent);

        this.authService = authService;
        this.toastr = toastr;
        this.pagamentoService = pagamentoService;
        this.novaSenha = "";
        this.novaSenha2 = "";
        this.statusPagamento = {
          "notificar_falta_pagamento": false,
          "pagamento_realizado": false,
          "subscription_id": null
        };
      }

      _createClass(TelaConfigComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this4 = this;

          this.authService.verifyToken().subscribe(function (resp) {
            var escritorio = resp.data.payload.escritorio_id;

            _this4.pagamentoService.getStatus(escritorio).subscribe(function (resp) {
              _this4.statusPagamento = resp.data;
              console.log(resp);
            });
          });
        }
      }, {
        key: "alterarNotificacaoPagamento",
        value: function alterarNotificacaoPagamento(notificar) {
          this.pagamentoService.notificarFaltaPagamento(notificar).subscribe();
        }
      }, {
        key: "alterar",
        value: function alterar() {
          var _this5 = this;

          if (this.novaSenha.length < 6) {
            this.toastr.warning("A senha deve ter pelo menos 6 caracteres");
            return;
          }

          if (this.novaSenha !== this.novaSenha2) {
            this.toastr.warning("As senhas não conferem");
            return;
          }

          this.authService.trocarSenha(this.novaSenha).subscribe(function (resp) {
            if (resp.success) {
              _this5.novaSenha = "";
              _this5.novaSenha2 = "";
            }
          });
        }
      }]);

      return TelaConfigComponent;
    }();

    TelaConfigComponent.ɵfac = function TelaConfigComponent_Factory(t) {
      return new (t || TelaConfigComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_pagamento_service__WEBPACK_IMPORTED_MODULE_3__["PagamentoService"]));
    };

    TelaConfigComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TelaConfigComponent,
      selectors: [["app-tela-config"]],
      decls: 16,
      vars: 3,
      consts: [[1, "row"], [1, "offset-lg-3"], [1, "col-lg-6"], [1, ""], ["type", "checkbox", 3, "ngModel", "ngModelChange", "click"], ["type", "password", "placeholder", "Nova Senha", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "password", "placeholder", "Confirme a Senha", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "button", 1, "btn", "btn-primary", "form-control", 3, "click"], [1, "fas", "fa-lock"]],
      template: function TelaConfigComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaConfigComponent_Template_input_ngModelChange_4_listener($event) {
            return ctx.statusPagamento.notificar_falta_pagamento = $event;
          })("click", function TelaConfigComponent_Template_input_click_4_listener($event) {
            return ctx.alterarNotificacaoPagamento($event.target.checked);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, " Notificar falta de pagamento");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Alterar Senha");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "input", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaConfigComponent_Template_input_ngModelChange_10_listener($event) {
            return ctx.novaSenha = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "input", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaConfigComponent_Template_input_ngModelChange_11_listener($event) {
            return ctx.novaSenha2 = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "button", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TelaConfigComponent_Template_button_click_12_listener() {
            return ctx.alterar();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "i", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " Trocar Senha ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.statusPagamento.notificar_falta_pagamento);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.novaSenha);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.novaSenha2);
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["CheckboxControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"]],
      styles: ["label[_ngcontent-%COMP%]{\n\tfont-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90ZWxhLWNvbmZpZy90ZWxhLWNvbmZpZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0NBQ0MsaUJBQWlCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy90ZWxhLWNvbmZpZy90ZWxhLWNvbmZpZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsibGFiZWx7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TelaConfigComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-tela-config',
          templateUrl: './tela-config.component.html',
          styleUrls: ['./tela-config.component.css']
        }]
      }], function () {
        return [{
          type: _services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]
        }, {
          type: ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]
        }, {
          type: _services_pagamento_service__WEBPACK_IMPORTED_MODULE_3__["PagamentoService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/tela-convite/tela-convite.component.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/components/tela-convite/tela-convite.component.ts ***!
    \*******************************************************************/

  /*! exports provided: TelaConviteComponent */

  /***/
  function srcAppComponentsTelaConviteTelaConviteComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TelaConviteComponent", function () {
      return TelaConviteComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _services_convite_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/convite.service */
    "./src/app/services/convite.service.ts");
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ngx-toastr */
    "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
    /* harmony import */


    var _services_usuario_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/usuario.service */
    "./src/app/services/usuario.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

    function TelaConviteComponent_div_1_Template(rf, ctx) {
      if (rf & 1) {
        var _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Email");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "input", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Nome");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "input", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaConviteComponent_div_1_Template_input_ngModelChange_6_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r23);

          var ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r22.nome = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Senha");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "input", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaConviteComponent_div_1_Template_input_ngModelChange_9_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r23);

          var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r24.senha = $event;
        })("keyup.enter", function TelaConviteComponent_div_1_Template_input_keyup_enter_9_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r23);

          var ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r25.cadastrar();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Confirme a Senha");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "input", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaConviteComponent_div_1_Template_input_ngModelChange_12_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r23);

          var ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r26.senha2 = $event;
        })("keyup.enter", function TelaConviteComponent_div_1_Template_input_keyup_enter_12_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r23);

          var ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r27.cadastrar();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TelaConviteComponent_div_1_Template_button_click_13_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r23);

          var ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r28.cadastrar();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "i", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, " Cadastrar ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("value", ctx_r20.convite.email);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r20.nome);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r20.senha);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r20.senha2);
      }
    }

    function TelaConviteComponent_div_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "CONVITE INV\xC1LIDO");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, " Por favor pe\xE7a ao administrador para enviar um novo convite. ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    var TelaConviteComponent =
    /*#__PURE__*/
    function () {
      function TelaConviteComponent(activatedRoute, conviteService, toastr, usuarioService, router) {
        _classCallCheck(this, TelaConviteComponent);

        this.activatedRoute = activatedRoute;
        this.conviteService = conviteService;
        this.toastr = toastr;
        this.usuarioService = usuarioService;
        this.router = router;
        this.nome = "";
        this.senha = "";
        this.senha2 = "";
        this.conviteInvalido = false;
        this.convite = {
          email: "",
          codigo: "",
          id: null
        };
      }

      _createClass(TelaConviteComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this6 = this;

          var codigo = this.activatedRoute.snapshot.params['codigo'];

          if (codigo) {
            this.conviteService.get(codigo).subscribe(function (res) {
              if (res.data) {
                _this6.convite = res.data;
              } else {
                _this6.conviteInvalido = true;
              }
            });
          } else {
            this.conviteInvalido = true;
          }
        }
      }, {
        key: "cadastrar",
        value: function cadastrar() {
          var _this7 = this;

          if (this.senha != this.senha2) {
            this.toastr.warning("As senhas não conferem");
            return;
          }

          if (this.nome.length < 3) {
            this.toastr.warning("Digite seu nome");
            return;
          }

          if (this.senha.length < 6) {
            this.toastr.warning("A senha deve ter ao menos 6 caracteres");
            return;
          }

          this.usuarioService.cadastrar(this.convite.codigo, this.nome, this.senha).subscribe(function (resp) {
            if (resp.success) {
              _this7.router.navigate(['login']);
            }
          });
        }
      }]);

      return TelaConviteComponent;
    }();

    TelaConviteComponent.ɵfac = function TelaConviteComponent_Factory(t) {
      return new (t || TelaConviteComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_convite_service__WEBPACK_IMPORTED_MODULE_2__["ConviteService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_usuario_service__WEBPACK_IMPORTED_MODULE_4__["UsuarioService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]));
    };

    TelaConviteComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TelaConviteComponent,
      selectors: [["app-tela-convite"]],
      decls: 3,
      vars: 2,
      consts: [[1, "flex-container"], ["class", "form-container col-10 col-sm-8 col-md-4 col-lg-3", 4, "ngIf"], [1, "form-container", "col-10", "col-sm-8", "col-md-4", "col-lg-3"], ["type", "email", "disabled", "", 1, "form-control", 3, "value"], ["type", "text", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "password", 1, "form-control", 3, "ngModel", "ngModelChange", "keyup.enter"], ["type", "button", "name", "button", 1, "btn", "btn-primary", "form-control", 3, "click"], [1, "fas", "fa-sign-in-alt"], [1, "invalid"], [1, "fas", "fa-exclamation-triangle"], [1, "invalid-header"]],
      template: function TelaConviteComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TelaConviteComponent_div_1_Template, 16, 4, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TelaConviteComponent_div_2_Template, 7, 0, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.conviteInvalido);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.conviteInvalido);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"]],
      styles: [".form-container[_ngcontent-%COMP%]{\n\tbackground-color: #ffffff;\n\tborder-radius: 15px;\n\tpadding: 15px;\n\talign-self: center;\n\tmargin: auto;\n}\n\n.form-button[_ngcontent-%COMP%]{\n\twidth: 100%;\n}\n\nlabel[_ngcontent-%COMP%]{\n\tfont-weight: bold;\n}\n\ninput[_ngcontent-%COMP%]{\n\tmargin-bottom: 15px;\n}\n\n.invalid[_ngcontent-%COMP%]{\n\tfont-size: 1000%;\n\tcolor: #fcba03;\n\ttext-align: center;\n}\n\n.invalid-header[_ngcontent-%COMP%]{\n\ttext-align: center;\n\tfont-size: 150%;\n\tfont-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90ZWxhLWNvbnZpdGUvdGVsYS1jb252aXRlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Q0FDQyx5QkFBeUI7Q0FDekIsbUJBQW1CO0NBQ25CLGFBQWE7Q0FDYixrQkFBa0I7Q0FDbEIsWUFBWTtBQUNiOztBQUVBO0NBQ0MsV0FBVztBQUNaOztBQUVBO0NBQ0MsaUJBQWlCO0FBQ2xCOztBQUNBO0NBQ0MsbUJBQW1CO0FBQ3BCOztBQUVBO0NBQ0MsZ0JBQWdCO0NBQ2hCLGNBQWM7Q0FDZCxrQkFBa0I7QUFDbkI7O0FBQ0E7Q0FDQyxrQkFBa0I7Q0FDbEIsZUFBZTtDQUNmLGlCQUFpQjtBQUNsQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGVsYS1jb252aXRlL3RlbGEtY29udml0ZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcm0tY29udGFpbmVye1xuXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuXHRib3JkZXItcmFkaXVzOiAxNXB4O1xuXHRwYWRkaW5nOiAxNXB4O1xuXHRhbGlnbi1zZWxmOiBjZW50ZXI7XG5cdG1hcmdpbjogYXV0bztcbn1cblxuLmZvcm0tYnV0dG9ue1xuXHR3aWR0aDogMTAwJTtcbn1cblxubGFiZWx7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuaW5wdXR7XG5cdG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG5cbi5pbnZhbGlke1xuXHRmb250LXNpemU6IDEwMDAlO1xuXHRjb2xvcjogI2ZjYmEwMztcblx0dGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmludmFsaWQtaGVhZGVye1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdGZvbnQtc2l6ZTogMTUwJTtcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TelaConviteComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-tela-convite',
          templateUrl: './tela-convite.component.html',
          styleUrls: ['./tela-convite.component.css']
        }]
      }], function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]
        }, {
          type: _services_convite_service__WEBPACK_IMPORTED_MODULE_2__["ConviteService"]
        }, {
          type: ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]
        }, {
          type: _services_usuario_service__WEBPACK_IMPORTED_MODULE_4__["UsuarioService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/tela-login/tela-login.component.ts":
  /*!***************************************************************!*\
    !*** ./src/app/components/tela-login/tela-login.component.ts ***!
    \***************************************************************/

  /*! exports provided: TelaLoginComponent */

  /***/
  function srcAppComponentsTelaLoginTelaLoginComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TelaLoginComponent", function () {
      return TelaLoginComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ngx-toastr */
    "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

    var TelaLoginComponent =
    /*#__PURE__*/
    function () {
      function TelaLoginComponent(auth, toastr, router, activatedRoute) {
        _classCallCheck(this, TelaLoginComponent);

        this.auth = auth;
        this.toastr = toastr;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.email = "";
        this.senha = "";
        this.isLoggedIn = false;
      }

      _createClass(TelaLoginComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this8 = this;

          var jwt = this.activatedRoute.snapshot.params['jwt'];

          if (jwt) {
            localStorage.setItem('jwt', jwt);
          }

          console.log(jwt);
          this.auth.verifyToken().subscribe(function (resp) {
            _this8.isLoggedIn = resp.data.isValid;

            if (_this8.isLoggedIn) {
              _this8.auth.loggedIn.emit(true);

              _this8.router.navigate(['termos']);
            }
          });
        }
      }, {
        key: "login",
        value: function login() {
          var _this9 = this;

          this.auth.getToken(this.email, this.senha).subscribe(function (resp) {
            if (resp.success) {
              localStorage.setItem("jwt", resp.data);

              _this9.router.navigate(['termos']);

              _this9.auth.loggedIn.emit(true);
            }
          });
        }
      }, {
        key: "esqueciSenha",
        value: function esqueciSenha() {
          if (this.email) {
            this.auth.resetarSenha(this.email).subscribe();
          } else {
            this.toastr.error("Digite seu email");
          }
        }
      }]);

      return TelaLoginComponent;
    }();

    TelaLoginComponent.ɵfac = function TelaLoginComponent_Factory(t) {
      return new (t || TelaLoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]));
    };

    TelaLoginComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TelaLoginComponent,
      selectors: [["app-tela-login"]],
      decls: 15,
      vars: 2,
      consts: [[1, "flex-container"], [1, "form-container", "col-10", "col-sm-8", "col-md-4", "col-lg-3"], ["type", "email", "aria-describedby", "emailHelp", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "password", "aria-describedby", "emailHelp", 1, "form-control", 3, "ngModel", "ngModelChange", "keyup.enter"], ["type", "button", 1, "btn", "btn-primary", "form-button", 3, "click"], [1, "fas", "fa-sign-in-alt"], [1, "esqueci-senha"], [3, "click"]],
      template: function TelaLoginComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Email");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaLoginComponent_Template_input_ngModelChange_4_listener($event) {
            return ctx.email = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Senha");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "input", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaLoginComponent_Template_input_ngModelChange_7_listener($event) {
            return ctx.senha = $event;
          })("keyup.enter", function TelaLoginComponent_Template_input_keyup_enter_7_listener() {
            return ctx.login();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "hr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TelaLoginComponent_Template_button_click_9_listener() {
            return ctx.login();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "i", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, " Entrar ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TelaLoginComponent_Template_span_click_13_listener() {
            return ctx.esqueciSenha();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " Esqueci a senha ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.email);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.senha);
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"]],
      styles: [".form-container[_ngcontent-%COMP%]{\n\tbackground-color: #ffffff;\n\tborder-radius: 15px;\n\tpadding: 15px;\n\talign-self: center;\n\tmargin: auto;\n}\n.form-button[_ngcontent-%COMP%]{\n\twidth: 100%;\n}\n.esqueci-senha[_ngcontent-%COMP%]{\n\ttext-decoration: underline;\n\n\tcolor: blue;\n\tmargin-top: 15px;\n}\n.esqueci-senha[_ngcontent-%COMP%]    > span[_ngcontent-%COMP%]{\n\tcursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90ZWxhLWxvZ2luL3RlbGEtbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSw4RUFBOEU7QUFDOUU7O0dBRUc7QUFDSDtDQUNDLHlCQUF5QjtDQUN6QixtQkFBbUI7Q0FDbkIsYUFBYTtDQUNiLGtCQUFrQjtDQUNsQixZQUFZO0FBQ2I7QUFFQTtDQUNDLFdBQVc7QUFDWjtBQUVBO0NBQ0MsMEJBQTBCOztDQUUxQixXQUFXO0NBQ1gsZ0JBQWdCO0FBQ2pCO0FBRUE7Q0FDQyxlQUFlO0FBQ2hCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy90ZWxhLWxvZ2luL3RlbGEtbG9naW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIFlvdSBjYW4gYWRkIGdsb2JhbCBzdHlsZXMgdG8gdGhpcyBmaWxlLCBhbmQgYWxzbyBpbXBvcnQgb3RoZXIgc3R5bGUgZmlsZXMgKi9cbi8qIGJvZHl7XG5cdGJhY2tncm91bmQtY29sb3I6IHJlZDtcbn0gKi9cbi5mb3JtLWNvbnRhaW5lcntcblx0YmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcblx0Ym9yZGVyLXJhZGl1czogMTVweDtcblx0cGFkZGluZzogMTVweDtcblx0YWxpZ24tc2VsZjogY2VudGVyO1xuXHRtYXJnaW46IGF1dG87XG59XG5cbi5mb3JtLWJ1dHRvbntcblx0d2lkdGg6IDEwMCU7XG59XG5cbi5lc3F1ZWNpLXNlbmhhe1xuXHR0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcblxuXHRjb2xvcjogYmx1ZTtcblx0bWFyZ2luLXRvcDogMTVweDtcbn1cblxuLmVzcXVlY2ktc2VuaGEgPiBzcGFue1xuXHRjdXJzb3I6IHBvaW50ZXI7XG59XG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TelaLoginComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-tela-login',
          templateUrl: './tela-login.component.html',
          styleUrls: ['./tela-login.component.css']
        }]
      }], function () {
        return [{
          type: _services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]
        }, {
          type: ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/tela-pagamento/tela-pagamento.component.ts":
  /*!***********************************************************************!*\
    !*** ./src/app/components/tela-pagamento/tela-pagamento.component.ts ***!
    \***********************************************************************/

  /*! exports provided: TelaPagamentoComponent */

  /***/
  function srcAppComponentsTelaPagamentoTelaPagamentoComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TelaPagamentoComponent", function () {
      return TelaPagamentoComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default =
    /*#__PURE__*/
    __webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ngx-toastr */
    "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
    /* harmony import */


    var _services_pagamento_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/pagamento.service */
    "./src/app/services/pagamento.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _pipes_credit_card_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../pipes/credit-card.pipe */
    "./src/app/pipes/credit-card.pipe.ts");
    /* harmony import */


    var _pipes_cpf_cnpj_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../pipes/cpf-cnpj.pipe */
    "./src/app/pipes/cpf-cnpj.pipe.ts"); //https://sweetalert2.github.io/	https://stackoverflow.com/questions/41684114/angular-2-easy-way-to-make-a-confirmation-dialog


    function TelaPagamentoComponent_span_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Ok! ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function TelaPagamentoComponent_span_8_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " N\xE3o realizado ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function TelaPagamentoComponent_span_10_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Realizar Pagamento ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function TelaPagamentoComponent_span_11_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Alterar Cart\xE3o ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function TelaPagamentoComponent_div_27_Template(rf, ctx) {
      if (rf & 1) {
        var _r35 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TelaPagamentoComponent_div_27_Template_button_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r35);

          var ctx_r34 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r34.cancelar();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Cancelar Pagamentos ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    var TelaPagamentoComponent =
    /*#__PURE__*/
    function () {
      function TelaPagamentoComponent(authService, toastr, pagamentoService) {
        _classCallCheck(this, TelaPagamentoComponent);

        this.authService = authService;
        this.toastr = toastr;
        this.pagamentoService = pagamentoService;
        this.statusPagamento = {
          "notificar_falta_pagamento": false,
          "pagamento_realizado": false,
          "subscription_id": null,
          "testando": false
        };
        this.subscription = {
          "card_number": "",
          "card_cvv": "",
          "card_holder_name": "",
          "card_expiration_date": "",
          "payment_method": "credit_card",
          "plan_id": "",
          "customer": {
            "email": "",
            "document_number": "",
            "name": ""
          }
        };
      }

      _createClass(TelaPagamentoComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this10 = this;

          this.authService.verifyToken().subscribe(function (resp) {
            var escritorio = resp.data.payload.escritorio_id;

            _this10.pagamentoService.getStatus(escritorio).subscribe(function (resp) {
              _this10.statusPagamento = resp.data;
              console.log(resp);
            });
          });
        }
      }, {
        key: "confirmar",
        value: function confirmar() {
          if (this.statusPagamento.pagamento_realizado && this.statusPagamento.subscription_id) {
            this.pagamentoService.alterarCartao(this.subscription).subscribe(function (resp) {
              console.log(resp);
            });
          } else {
            this.pagamentoService.criarAssinatura(this.subscription).subscribe(function (resp) {
              console.log(resp);
            });
          }
        }
      }, {
        key: "cancelar",
        value: function cancelar() {
          var _this11 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
            title: 'Tem certeza?',
            text: 'Seu escritório não receberá mais nenhum resumo diário',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não'
          }).then(function (result) {
            if (result.value) {
              _this11.pagamentoService.cancelarAssinatura().subscribe(function (resp) {
                console.log(resp);
              });
            }
          });
        }
      }]);

      return TelaPagamentoComponent;
    }();

    TelaPagamentoComponent.ɵfac = function TelaPagamentoComponent_Factory(t) {
      return new (t || TelaPagamentoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_pagamento_service__WEBPACK_IMPORTED_MODULE_4__["PagamentoService"]));
    };

    TelaPagamentoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TelaPagamentoComponent,
      selectors: [["app-tela-pagamento"]],
      decls: 29,
      vars: 18,
      consts: [[1, "row"], [1, "offset-lg-3"], [1, "col-lg-6"], [1, ""], [4, "ngIf"], ["type", "text", "placeholder", "N\xFAmero do Cart\xE3o", "maxlength", "19", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "text", "placeholder", "Nome Impresso no Cart\xE3o", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "text", "placeholder", "CVV", "maxlength", "3", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "text", "maxlength", "5", "placeholder", "Validade do Cart\xE3o", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "text", "placeholder", "Seu Nome", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "text", "placeholder", "Email", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "text", "placeholder", "N\xFAmero do Documento (CPF/CNPJ)", "maxlength", "18", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "button", 1, "btn", "btn-primary", "form-control", 3, "click"], ["type", "button", 1, "btn", "btn-danger", "form-control", 3, "click"]],
      template: function TelaPagamentoComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Status:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TelaPagamentoComponent_span_7_Template, 2, 0, "span", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, TelaPagamentoComponent_span_8_Template, 2, 0, "span", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, TelaPagamentoComponent_span_10_Template, 3, 0, "span", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, TelaPagamentoComponent_span_11_Template, 3, 0, "span", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "input", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaPagamentoComponent_Template_input_ngModelChange_13_listener($event) {
            return ctx.subscription.card_number = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](14, "ccnumber");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "input", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaPagamentoComponent_Template_input_ngModelChange_15_listener($event) {
            return ctx.subscription.card_holder_name = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "input", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaPagamentoComponent_Template_input_ngModelChange_16_listener($event) {
            return ctx.subscription.card_cvv = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "input", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaPagamentoComponent_Template_input_ngModelChange_17_listener($event) {
            return ctx.subscription.card_expiration_date = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](18, "ccexp");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "hr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "input", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaPagamentoComponent_Template_input_ngModelChange_20_listener($event) {
            return ctx.subscription.customer.name = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "input", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaPagamentoComponent_Template_input_ngModelChange_21_listener($event) {
            return ctx.subscription.customer.email = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "input", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaPagamentoComponent_Template_input_ngModelChange_22_listener($event) {
            return ctx.subscription.customer.document_number = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](23, "cpfcnpj");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TelaPagamentoComponent_Template_button_click_24_listener() {
            return ctx.confirmar();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, " Confirmar ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "hr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, TelaPagamentoComponent_div_27_Template, 3, 0, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.statusPagamento.pagamento_realizado);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.statusPagamento.pagamento_realizado);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.statusPagamento.subscription_id);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.statusPagamento.subscription_id);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](14, 12, ctx.subscription.card_number));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.subscription.card_holder_name);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.subscription.card_cvv);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](18, 14, ctx.subscription.card_expiration_date));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.subscription.customer.name);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.subscription.customer.email);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](23, 16, ctx.subscription.customer.document_number));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.statusPagamento.subscription_id);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["MaxLengthValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"]],
      pipes: [_pipes_credit_card_pipe__WEBPACK_IMPORTED_MODULE_7__["CreditCardNumberPipe"], _pipes_credit_card_pipe__WEBPACK_IMPORTED_MODULE_7__["CreditCardExpirationPipe"], _pipes_cpf_cnpj_pipe__WEBPACK_IMPORTED_MODULE_8__["CpfCnpjPipe"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGVsYS1wYWdhbWVudG8vdGVsYS1wYWdhbWVudG8uY29tcG9uZW50LmNzcyJ9 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TelaPagamentoComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-tela-pagamento',
          templateUrl: './tela-pagamento.component.html',
          styleUrls: ['./tela-pagamento.component.css']
        }]
      }], function () {
        return [{
          type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
        }, {
          type: ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]
        }, {
          type: _services_pagamento_service__WEBPACK_IMPORTED_MODULE_4__["PagamentoService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/tela-termos/tela-termos.component.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/components/tela-termos/tela-termos.component.ts ***!
    \*****************************************************************/

  /*! exports provided: TelaTermosComponent */

  /***/
  function srcAppComponentsTelaTermosTelaTermosComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TelaTermosComponent", function () {
      return TelaTermosComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _termos_manager_admin_termos_manager_admin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../termos-manager-admin/termos-manager-admin.component */
    "./src/app/components/termos-manager-admin/termos-manager-admin.component.ts");
    /* harmony import */


    var _termos_manager_user_termos_manager_user_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../termos-manager-user/termos-manager-user.component */
    "./src/app/components/termos-manager-user/termos-manager-user.component.ts");

    function TelaTermosComponent_app_termos_manager_admin_0_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-termos-manager-admin");
      }
    }

    function TelaTermosComponent_app_termos_manager_user_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-termos-manager-user");
      }
    }

    var TelaTermosComponent =
    /*#__PURE__*/
    function () {
      function TelaTermosComponent(authService) {
        _classCallCheck(this, TelaTermosComponent);

        this.authService = authService;
        this.isAdmin = false;
      }

      _createClass(TelaTermosComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this12 = this;

          this.authService.verifyToken().subscribe(function (resp) {
            if (resp.success && resp.data.isValid) {
              _this12.isAdmin = resp.data.payload.admin;
            }
          });
        }
      }]);

      return TelaTermosComponent;
    }();

    TelaTermosComponent.ɵfac = function TelaTermosComponent_Factory(t) {
      return new (t || TelaTermosComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]));
    };

    TelaTermosComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TelaTermosComponent,
      selectors: [["app-tela-termos"]],
      decls: 2,
      vars: 2,
      consts: [[4, "ngIf"]],
      template: function TelaTermosComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, TelaTermosComponent_app_termos_manager_admin_0_Template, 1, 0, "app-termos-manager-admin", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TelaTermosComponent_app_termos_manager_user_1_Template, 1, 0, "app-termos-manager-user", 0);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isAdmin);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isAdmin);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _termos_manager_admin_termos_manager_admin_component__WEBPACK_IMPORTED_MODULE_3__["TermosManagerAdminComponent"], _termos_manager_user_termos_manager_user_component__WEBPACK_IMPORTED_MODULE_4__["TermosManagerUserComponent"]],
      styles: [".seguidor[_ngcontent-%COMP%]{\n\tmargin-left: 10px;\n}\n.remove-usuario[_ngcontent-%COMP%]{\n\tmargin-left: 5px;\n\tmargin-right: 10px;\n\tfont-size: 120%;\n}\n.pointer[_ngcontent-%COMP%]{\n\tcursor:pointer;\n}\nlabel[_ngcontent-%COMP%]{\n\tfont-weight: bold;\t\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90ZWxhLXRlcm1vcy90ZWxhLXRlcm1vcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0NBQ0MsaUJBQWlCO0FBQ2xCO0FBQ0E7Q0FDQyxnQkFBZ0I7Q0FDaEIsa0JBQWtCO0NBQ2xCLGVBQWU7QUFDaEI7QUFFQTtDQUNDLGNBQWM7QUFDZjtBQUVBO0NBQ0MsaUJBQWlCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy90ZWxhLXRlcm1vcy90ZWxhLXRlcm1vcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNlZ3VpZG9ye1xuXHRtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5yZW1vdmUtdXN1YXJpb3tcblx0bWFyZ2luLWxlZnQ6IDVweDtcblx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xuXHRmb250LXNpemU6IDEyMCU7XG59XG5cbi5wb2ludGVye1xuXHRjdXJzb3I6cG9pbnRlcjtcbn1cblxubGFiZWx7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1x0XG59XG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TelaTermosComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-tela-termos',
          templateUrl: './tela-termos.component.html',
          styleUrls: ['./tela-termos.component.css']
        }]
      }], function () {
        return [{
          type: _services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/tela-usuarios/tela-usuarios.component.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/components/tela-usuarios/tela-usuarios.component.ts ***!
    \*********************************************************************/

  /*! exports provided: TelaUsuariosComponent */

  /***/
  function srcAppComponentsTelaUsuariosTelaUsuariosComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TelaUsuariosComponent", function () {
      return TelaUsuariosComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default =
    /*#__PURE__*/
    __webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
    /* harmony import */


    var _services_usuario_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/usuario.service */
    "./src/app/services/usuario.service.ts");
    /* harmony import */


    var _services_termos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/termos.service */
    "./src/app/services/termos.service.ts");
    /* harmony import */


    var _services_convite_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/convite.service */
    "./src/app/services/convite.service.ts");
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-toastr */
    "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js"); //https://sweetalert2.github.io/	https://stackoverflow.com/questions/41684114/angular-2-easy-way-to-make-a-confirmation-dialog


    function TelaUsuariosComponent_div_11_span_8_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var usuario_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](usuario_r3.nome);
      }
    }

    function TelaUsuariosComponent_div_11_span_9_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "N\xE3o cadastrado");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function TelaUsuariosComponent_div_11_span_11_Template(rf, ctx) {
      if (rf & 1) {
        var _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TelaUsuariosComponent_div_11_span_11_Template_span_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11);

          var usuario_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r10.novoUsuario = usuario_r3.email;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "i", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function TelaUsuariosComponent_div_11_div_16_Template(rf, ctx) {
      if (rf & 1) {
        var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TelaUsuariosComponent_div_11_div_16_Template_span_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

          var termo_r13 = ctx.$implicit;

          var usuario_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r14.abandonaTermo(termo_r13, usuario_r3);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var termo_r13 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", termo_r13.termo, " ");
      }
    }

    function TelaUsuariosComponent_div_11_div_17_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " N\xE3o acompanha nenhum termo ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function TelaUsuariosComponent_div_11_Template(rf, ctx) {
      if (rf & 1) {
        var _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TelaUsuariosComponent_div_11_Template_div_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r18);

          var usuario_r3 = ctx.$implicit;

          var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r17.detalharUsuario(usuario_r3.id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "i", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, TelaUsuariosComponent_div_11_span_8_Template, 2, 1, "span", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TelaUsuariosComponent_div_11_span_9_Template, 2, 0, "span", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, TelaUsuariosComponent_div_11_span_11_Template, 2, 0, "span", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TelaUsuariosComponent_div_11_Template_span_click_13_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r18);

          var usuario_r3 = ctx.$implicit;

          var ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r19["delete"](usuario_r3);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "i", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, TelaUsuariosComponent_div_11_div_16_Template, 4, 1, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, TelaUsuariosComponent_div_11_div_17_Template, 2, 0, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var usuario_r3 = ctx.$implicit;

        var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r2.idUsuarioDetalhado != usuario_r3.id);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r2.idUsuarioDetalhado == usuario_r3.id);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", usuario_r3.email, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", usuario_r3.cadastrado);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !usuario_r3.cadastrado);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !usuario_r3.cadastrado);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r2.idUsuarioDetalhado != usuario_r3.id);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", usuario_r3.termos);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", usuario_r3.termos.length == 0);
      }
    }

    var TelaUsuariosComponent =
    /*#__PURE__*/
    function () {
      function TelaUsuariosComponent(usuarioService, termoService, conviteService, toastr) {
        _classCallCheck(this, TelaUsuariosComponent);

        this.usuarioService = usuarioService;
        this.termoService = termoService;
        this.conviteService = conviteService;
        this.toastr = toastr;
        this.novoUsuario = "";
        this.usuarios = [];
        this.idUsuarioDetalhado = 0;
      }

      _createClass(TelaUsuariosComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.atualizarUsuarios();
        }
      }, {
        key: "convidar",
        value: function convidar() {
          var _this13 = this;

          if (this.novoUsuario.length < 3) {
            this.toastr.error("Email inválido");
            return;
          }

          if (!this.novoUsuario.includes("@")) {
            this.toastr.error("Email inválido");
            return;
          }

          this.conviteService.enviar(this.novoUsuario).subscribe(function (resp) {
            if (resp.success) {
              _this13.novoUsuario = "";

              _this13.atualizarUsuarios();
            }
          });
        }
      }, {
        key: "atualizarUsuarios",
        value: function atualizarUsuarios() {
          var _this14 = this;

          this.usuarioService.getAll().subscribe(function (resp) {
            if (resp.success) {
              _this14.usuarios = resp.data;
              console.log(_this14.usuarios);
            }
          });
        }
      }, {
        key: "delete",
        value: function _delete(user) {
          var _this15 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
            title: 'Tem certeza?',
            text: 'O usuario com email ' + user.email + ' será deletado. Isso significa que ele não receberá mais nenhum alerta e perderá acesso ao sistema (embora você possa enviar um novo convite)',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Prosseguir',
            cancelButtonText: 'Cancelar'
          }).then(function (result) {
            if (result.value) {
              _this15.usuarioService["delete"](user.id).subscribe(function (resp) {
                if (resp.success) {
                  _this15.atualizarUsuarios();
                }
              });
            }
          });
        }
      }, {
        key: "abandonaTermo",
        value: function abandonaTermo(termo, usuario) {
          var _this16 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
            title: 'Tem certeza?',
            text: 'O email ' + usuario.email + ' não receberá mais notificações para o termo "' + termo.termo + '"',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Prosseguir',
            cancelButtonText: 'Cancelar'
          }).then(function (result) {
            if (result.value) {
              _this16.termoService.pararAcompanhamentoAdm(termo.id, [{
                "id": usuario.id
              }]).subscribe(function (resp) {
                if (resp.success) {
                  _this16.atualizarUsuarios();
                }
              });
            }
          });
        }
      }, {
        key: "detalharUsuario",
        value: function detalharUsuario(id) {
          if (id == this.idUsuarioDetalhado) {
            this.idUsuarioDetalhado = 0;
          } else {
            this.idUsuarioDetalhado = id;
          }
        }
      }]);

      return TelaUsuariosComponent;
    }();

    TelaUsuariosComponent.ɵfac = function TelaUsuariosComponent_Factory(t) {
      return new (t || TelaUsuariosComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_usuario_service__WEBPACK_IMPORTED_MODULE_2__["UsuarioService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_termos_service__WEBPACK_IMPORTED_MODULE_3__["TermosService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_convite_service__WEBPACK_IMPORTED_MODULE_4__["ConviteService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]));
    };

    TelaUsuariosComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TelaUsuariosComponent,
      selectors: [["app-tela-usuarios"]],
      decls: 12,
      vars: 2,
      consts: [[1, "row"], [1, "col-lg-4"], ["type", "text", "placeholder", "Email", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "button", "name", "button", 1, "btn", "btn-primary", "form-control", 3, "click"], [1, "fas", "fa-envelope"], [1, "col-lg-8"], ["class", "row user-row", 4, "ngFor", "ngForOf"], [1, "row", "user-row"], [1, "col-5", "pointer", 2, "word-break", "break-all", 3, "click"], [3, "hidden"], [1, "fas", "fa-caret-down"], [1, "fas", "fa-caret-right"], [1, "col-4"], [4, "ngIf"], ["style", "font-style:italic", 4, "ngIf"], [1, "col-1"], ["class", "pointer", 3, "click", 4, "ngIf"], [1, "pointer", 3, "click"], [1, "fas", "fa-trash-alt"], [1, "col-12", 3, "hidden"], [4, "ngFor", "ngForOf"], ["class", "abandona-termo", "style", "font-style:italic", 4, "ngIf"], [2, "font-style", "italic"], [1, "abandona-termo", "pointer", 3, "click"], [1, "fas", "fa-times"], [1, "abandona-termo", 2, "font-style", "italic"]],
      template: function TelaUsuariosComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Convidar Usu\xE1rio");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TelaUsuariosComponent_Template_input_ngModelChange_4_listener($event) {
            return ctx.novoUsuario = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TelaUsuariosComponent_Template_button_click_5_listener() {
            return ctx.convidar();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "i", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " Convidar ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Usu\xE1rios Cadastrados");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, TelaUsuariosComponent_div_11_Template, 18, 9, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.novoUsuario);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.usuarios);
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"]],
      styles: ["label[_ngcontent-%COMP%]{\n\tfont-weight: bold;\n}\n\n.abandona-termo[_ngcontent-%COMP%]{\n\tmargin-left: 15px;\n\tmargin-right: 5px;\n}\n\n@media (max-width: 576px) {\n\n\t.user-row[_ngcontent-%COMP%]{\n\t\tborder-bottom-style: dotted;\n\t}\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90ZWxhLXVzdWFyaW9zL3RlbGEtdXN1YXJpb3MuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtDQUNDLGlCQUFpQjtBQUNsQjs7QUFFQTtDQUNDLGlCQUFpQjtDQUNqQixpQkFBaUI7QUFDbEI7O0FBR0E7O0NBRUM7RUFDQywyQkFBMkI7Q0FDNUI7QUFDRCIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGVsYS11c3Vhcmlvcy90ZWxhLXVzdWFyaW9zLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJsYWJlbHtcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5hYmFuZG9uYS10ZXJtb3tcblx0bWFyZ2luLWxlZnQ6IDE1cHg7XG5cdG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG5cbkBtZWRpYSAobWF4LXdpZHRoOiA1NzZweCkge1xuXG5cdC51c2VyLXJvd3tcblx0XHRib3JkZXItYm90dG9tLXN0eWxlOiBkb3R0ZWQ7XG5cdH1cbn1cbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TelaUsuariosComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-tela-usuarios',
          templateUrl: './tela-usuarios.component.html',
          styleUrls: ['./tela-usuarios.component.css']
        }]
      }], function () {
        return [{
          type: _services_usuario_service__WEBPACK_IMPORTED_MODULE_2__["UsuarioService"]
        }, {
          type: _services_termos_service__WEBPACK_IMPORTED_MODULE_3__["TermosService"]
        }, {
          type: _services_convite_service__WEBPACK_IMPORTED_MODULE_4__["ConviteService"]
        }, {
          type: ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/termos-manager-admin/termos-manager-admin.component.ts":
  /*!***********************************************************************************!*\
    !*** ./src/app/components/termos-manager-admin/termos-manager-admin.component.ts ***!
    \***********************************************************************************/

  /*! exports provided: TermosManagerAdminComponent */

  /***/
  function srcAppComponentsTermosManagerAdminTermosManagerAdminComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TermosManagerAdminComponent", function () {
      return TermosManagerAdminComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default =
    /*#__PURE__*/
    __webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ngx-toastr */
    "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
    /* harmony import */


    var _services_termos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/termos.service */
    "./src/app/services/termos.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js"); //https://sweetalert2.github.io/	https://stackoverflow.com/questions/41684114/angular-2-easy-way-to-make-a-confirmation-dialog


    var _c0 = ["termosContainer"];

    function TermosManagerAdminComponent_div_5_Template(rf, ctx) {
      if (rf & 1) {
        var _r60 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "input", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TermosManagerAdminComponent_div_5_Template_button_click_3_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r60);

          var email_r58 = ctx.$implicit;

          var ctx_r59 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r59.removerEmail(email_r58);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "i", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var email_r58 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("value", email_r58);
      }
    }

    function TermosManagerAdminComponent_div_17_div_12_Template(rf, ctx) {
      if (rf & 1) {
        var _r67 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TermosManagerAdminComponent_div_17_div_12_Template_span_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r67);

          var seguindo_r64 = ctx.$implicit;

          var termo_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r65 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r65.removerSeguidor(seguindo_r64, termo_r61);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var seguindo_r64 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", seguindo_r64.email, " ");
      }
    }

    function TermosManagerAdminComponent_div_17_div_13_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Ningu\xE9m acompanhando");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function TermosManagerAdminComponent_div_17_Template(rf, ctx) {
      if (rf & 1) {
        var _r69 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TermosManagerAdminComponent_div_17_Template_div_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r69);

          var termo_r61 = ctx.$implicit;

          var ctx_r68 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r68.exibirSeguidores(termo_r61.termo_id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TermosManagerAdminComponent_div_17_Template_div_click_3_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r69);

          var termo_r61 = ctx.$implicit;

          var ctx_r70 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r70.exibirSeguidores(termo_r61.termo_id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "i", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "span", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "i", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "span", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TermosManagerAdminComponent_div_17_Template_span_click_9_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r69);

          var termo_r61 = ctx.$implicit;

          var ctx_r71 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r71.deletarTermo(termo_r61);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "i", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, TermosManagerAdminComponent_div_17_div_12_Template, 4, 1, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, TermosManagerAdminComponent_div_17_div_13_Template, 3, 0, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var termo_r61 = ctx.$implicit;

        var ctx_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", termo_r61.termo_texto, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r57.idSeguindoExibido != termo_r61.termo_id);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r57.idSeguindoExibido == termo_r61.termo_id);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r57.idSeguindoExibido != termo_r61.termo_id);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", termo_r61.seguindo);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", termo_r61.seguindo.length == 0);
      }
    }

    var TermosManagerAdminComponent =
    /*#__PURE__*/
    function () {
      function TermosManagerAdminComponent(toastr, termoService) {
        _classCallCheck(this, TermosManagerAdminComponent);

        this.toastr = toastr;
        this.termoService = termoService;
        this.novoTermo = "";
        this.novoEmail = "";
        this.emails = [];
        this.termosSeguidos = [];
        this.termoHtmlTemplate = "";
        this.idSeguindoExibido = null;
      }

      _createClass(TermosManagerAdminComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.buscarTermosAtuais();
        }
      }, {
        key: "adicionarEmail",
        value: function adicionarEmail() {
          if (this.novoEmail.length < 3) {
            this.toastr.error("Email inválido");
            return;
          }

          if (!this.novoEmail.includes("@")) {
            this.toastr.error("Email inválido");
            return;
          }

          this.emails.push(this.novoEmail);
          this.novoEmail = '';
        }
      }, {
        key: "removerEmail",
        value: function removerEmail(email) {
          var index = this.emails.indexOf(email);

          if (index !== -1) {
            this.emails.splice(index, 1);
          }
        }
      }, {
        key: "salvarTermos",
        value: function salvarTermos() {
          var _this17 = this;

          this.termoService.insert(this.novoTermo, this.emails).subscribe(function (resp) {
            if (resp.success) {
              _this17.emails = [];
              _this17.novoTermo = '';

              _this17.buscarTermosAtuais();
            }
          });
        }
      }, {
        key: "buscarTermosAtuais",
        value: function buscarTermosAtuais() {
          var _this18 = this;

          this.termoService.listar().subscribe(function (resp) {
            if (resp.success) {
              _this18.termosSeguidos = resp.data;
            }
          });
        }
      }, {
        key: "exibirSeguidores",
        value: function exibirSeguidores(id) {
          if (id == this.idSeguindoExibido) {
            this.idSeguindoExibido = null;
          } else {
            this.idSeguindoExibido = id;
          }
        }
      }, {
        key: "removerSeguidor",
        value: function removerSeguidor(seguidor, termo) {
          var _this19 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
            title: 'Tem certeza?',
            text: 'O email ' + seguidor.email + ' não receberá mais alertas para o termo "' + termo.termo_texto + '"',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Prosseguir',
            cancelButtonText: 'Cancelar'
          }).then(function (result) {
            if (result.value) {
              _this19.termoService.pararAcompanhamentoAdm(termo.termo_id, [{
                "id": seguidor.id
              }]).subscribe(function (resp) {
                if (resp.success) {
                  _this19.buscarTermosAtuais();
                }
              });
            }
          });
        }
      }, {
        key: "deletarTermo",
        value: function deletarTermo(termo) {
          var _this20 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
            title: 'Tem certeza?',
            text: 'O termo "' + termo.termo_texto + '" será deletado e nenhum usuário receberá notificações sobre este termo',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Prosseguir',
            cancelButtonText: 'Cancelar'
          }).then(function (result) {
            if (result.value) {
              _this20.termoService["delete"](termo.termo_id).subscribe(function (resp) {
                if (resp.success) {
                  _this20.buscarTermosAtuais();
                }
              });
            }
          });
        }
      }]);

      return TermosManagerAdminComponent;
    }();

    TermosManagerAdminComponent.ɵfac = function TermosManagerAdminComponent_Factory(t) {
      return new (t || TermosManagerAdminComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_termos_service__WEBPACK_IMPORTED_MODULE_3__["TermosService"]));
    };

    TermosManagerAdminComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TermosManagerAdminComponent,
      selectors: [["app-termos-manager-admin"]],
      viewQuery: function TermosManagerAdminComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.termosContainer = _t.first);
        }
      },
      decls: 18,
      vars: 4,
      consts: [[1, "row"], [1, "col-lg-6"], ["type", "text", "placeholder", "Termo", 1, "form-control", 3, "ngModel", "ngModelChange"], ["class", "input-group", 4, "ngFor", "ngForOf"], [1, "input-group"], ["type", "text", "placeholder", "Email de alerta", 1, "form-control", 3, "ngModel", "ngModelChange", "keyup.enter"], [1, "input-group-append"], ["type", "button", 1, "btn", "btn-secondary", 3, "click"], [1, "fas", "fa-plus"], ["type", "button", 1, "btn", "btn-primary", "form-control", 3, "click"], [1, "fas", "fa-save"], ["class", "row", 4, "ngFor", "ngForOf"], ["type", "text", "disabled", "", 1, "form-control", 3, "value"], [1, "fas", "fa-minus"], [1, "col-9", "pointer", 3, "click"], [1, "col-1", "pointer", 3, "click"], [3, "hidden"], [1, "fas", "fa-caret-up"], [1, "fas", "fa-caret-down"], [1, "col-1", "pointer"], [3, "click"], [1, "fas", "fa-trash-alt"], [1, "col-12", 3, "hidden"], ["class", "seguidor", 4, "ngFor", "ngForOf"], ["class", "seguidor", 4, "ngIf"], [1, "seguidor"], ["data-toggle", "modal", 1, "remove-usuario", "pointer", 3, "click"], [1, "fas", "fa-times"], [2, "font-style", "italic", "font-size", "90%"]],
      template: function TermosManagerAdminComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Adicionar Termo");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TermosManagerAdminComponent_Template_input_ngModelChange_4_listener($event) {
            return ctx.novoTermo = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TermosManagerAdminComponent_div_5_Template, 5, 1, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "input", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TermosManagerAdminComponent_Template_input_ngModelChange_7_listener($event) {
            return ctx.novoEmail = $event;
          })("keyup.enter", function TermosManagerAdminComponent_Template_input_keyup_enter_7_listener() {
            return ctx.adicionarEmail();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TermosManagerAdminComponent_Template_button_click_9_listener() {
            return ctx.adicionarEmail();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "i", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TermosManagerAdminComponent_Template_button_click_11_listener() {
            return ctx.salvarTermos();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "i", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, " Salvar ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Termos Cadastrados");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, TermosManagerAdminComponent_div_17_Template, 14, 6, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.novoTermo);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.emails);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.novoEmail);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.termosSeguidos);
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"]],
      styles: [".seguidor[_ngcontent-%COMP%]{\n\tmargin-left: 10px;\n}\n.remove-usuario[_ngcontent-%COMP%]{\n\tmargin-left: 5px;\n\tmargin-right: 10px;\n\tfont-size: 120%;\n}\n.pointer[_ngcontent-%COMP%]{\n\tcursor:pointer;\n}\nlabel[_ngcontent-%COMP%]{\n\tfont-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90ZXJtb3MtbWFuYWdlci1hZG1pbi90ZXJtb3MtbWFuYWdlci1hZG1pbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0NBQ0MsaUJBQWlCO0FBQ2xCO0FBQ0E7Q0FDQyxnQkFBZ0I7Q0FDaEIsa0JBQWtCO0NBQ2xCLGVBQWU7QUFDaEI7QUFFQTtDQUNDLGNBQWM7QUFDZjtBQUVBO0NBQ0MsaUJBQWlCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy90ZXJtb3MtbWFuYWdlci1hZG1pbi90ZXJtb3MtbWFuYWdlci1hZG1pbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNlZ3VpZG9ye1xuXHRtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5yZW1vdmUtdXN1YXJpb3tcblx0bWFyZ2luLWxlZnQ6IDVweDtcblx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xuXHRmb250LXNpemU6IDEyMCU7XG59XG5cbi5wb2ludGVye1xuXHRjdXJzb3I6cG9pbnRlcjtcbn1cblxubGFiZWx7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TermosManagerAdminComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-termos-manager-admin',
          templateUrl: './termos-manager-admin.component.html',
          styleUrls: ['./termos-manager-admin.component.css']
        }]
      }], function () {
        return [{
          type: ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]
        }, {
          type: _services_termos_service__WEBPACK_IMPORTED_MODULE_3__["TermosService"]
        }];
      }, {
        termosContainer: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['termosContainer']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/components/termos-manager-user/termos-manager-user.component.ts":
  /*!*********************************************************************************!*\
    !*** ./src/app/components/termos-manager-user/termos-manager-user.component.ts ***!
    \*********************************************************************************/

  /*! exports provided: TermosManagerUserComponent */

  /***/
  function srcAppComponentsTermosManagerUserTermosManagerUserComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TermosManagerUserComponent", function () {
      return TermosManagerUserComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ngx-toastr */
    "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
    /* harmony import */


    var _services_termos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/termos.service */
    "./src/app/services/termos.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function TermosManagerUserComponent_tr_15_Template(rf, ctx) {
      if (rf & 1) {
        var _r75 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TermosManagerUserComponent_tr_15_Template_span_click_4_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r75);

          var termo_r73 = ctx.$implicit;

          var ctx_r74 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r74.abandonar(termo_r73.id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "i", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var termo_r73 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](termo_r73.termo_texto);
      }
    }

    var TermosManagerUserComponent =
    /*#__PURE__*/
    function () {
      function TermosManagerUserComponent(toastr, termoService) {
        _classCallCheck(this, TermosManagerUserComponent);

        this.toastr = toastr;
        this.termoService = termoService;
        this.novoTermo = '';
        this.termos = [];
      }

      _createClass(TermosManagerUserComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.atualizarTermos();
        }
      }, {
        key: "atualizarTermos",
        value: function atualizarTermos() {
          var _this21 = this;

          this.termoService.listar().subscribe(function (resp) {
            if (resp.success) {
              _this21.termos = resp.data;
            }
          });
        }
      }, {
        key: "acompanhar",
        value: function acompanhar() {
          var _this22 = this;

          this.termoService.insert(this.novoTermo, null).subscribe(function (resp) {
            if (resp.success) {
              _this22.atualizarTermos();

              _this22.novoTermo = '';
            }
          });
        }
      }, {
        key: "abandonar",
        value: function abandonar(id) {
          var _this23 = this;

          this.termoService.pararAcompanhamentoUser(id).subscribe(function (resp) {
            if (resp.success) {
              _this23.atualizarTermos();
            }
          });
        }
      }]);

      return TermosManagerUserComponent;
    }();

    TermosManagerUserComponent.ɵfac = function TermosManagerUserComponent_Factory(t) {
      return new (t || TermosManagerUserComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_termos_service__WEBPACK_IMPORTED_MODULE_2__["TermosService"]));
    };

    TermosManagerUserComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TermosManagerUserComponent,
      selectors: [["app-termos-manager-user"]],
      decls: 17,
      vars: 2,
      consts: [[1, "row"], [1, "offset-lg-3"], [1, "col-lg-6"], [1, "input-group"], ["type", "text", "placeholder", "Acompanhar novo termo", 1, "form-control", 3, "ngModel", "ngModelChange", "keyup.enter"], [1, "input-group-append"], ["type", "button", 1, "btn", "btn-secondary", 3, "click"], [1, "table-responsive"], [1, "table", "table-hover"], [4, "ngFor", "ngForOf"], [2, "width", "80%"], [1, "text-right"], [1, "pointer", 3, "click"], [1, "fas", "fa-times"]],
      template: function TermosManagerUserComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TermosManagerUserComponent_Template_input_ngModelChange_4_listener($event) {
            return ctx.novoTermo = $event;
          })("keyup.enter", function TermosManagerUserComponent_Template_input_keyup_enter_4_listener() {
            return ctx.acompanhar();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TermosManagerUserComponent_Template_button_click_6_listener() {
            return ctx.acompanhar();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " Acompanhar ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "table", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "tbody");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, TermosManagerUserComponent_tr_15_Template, 6, 1, "tr", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.novoTermo);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.termos);
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"]],
      styles: ["label[_ngcontent-%COMP%]{\n\tfont-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90ZXJtb3MtbWFuYWdlci11c2VyL3Rlcm1vcy1tYW5hZ2VyLXVzZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtDQUNDLGlCQUFpQjtBQUNsQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGVybW9zLW1hbmFnZXItdXNlci90ZXJtb3MtbWFuYWdlci11c2VyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJsYWJlbHtcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TermosManagerUserComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-termos-manager-user',
          templateUrl: './termos-manager-user.component.html',
          styleUrls: ['./termos-manager-user.component.css']
        }]
      }], function () {
        return [{
          type: ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"]
        }, {
          type: _services_termos_service__WEBPACK_IMPORTED_MODULE_2__["TermosService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/pipes/cpf-cnpj.pipe.ts":
  /*!****************************************!*\
    !*** ./src/app/pipes/cpf-cnpj.pipe.ts ***!
    \****************************************/

  /*! exports provided: CpfCnpjPipe */

  /***/
  function srcAppPipesCpfCnpjPipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CpfCnpjPipe", function () {
      return CpfCnpjPipe;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var CpfCnpjPipe =
    /*#__PURE__*/
    function () {
      function CpfCnpjPipe() {
        _classCallCheck(this, CpfCnpjPipe);
      }

      _createClass(CpfCnpjPipe, [{
        key: "transform",
        value: function transform(txt) {
          txt = txt.replace(/[^0-9]/g, "");
          var masked = "";

          if (txt.length <= 11) {
            for (var i = 1; i < txt.length + 1; i++) {
              masked = masked + txt[i - 1];

              if (i % 3 == 0) {
                masked = masked + ".";
              }
            }

            var count = (masked.match(/\./g) || []).length;

            if (count == 3) {
              var pos = masked.lastIndexOf('.');
              masked = masked.substring(0, pos) + '-' + masked.substring(pos + 1);
            }

            return masked;
          } else {
            masked = txt.replace(/(.{2})(.*)/, "$1.$2");
            masked = masked.replace(/(.{6})(.*)/, "$1.$2");
            masked = masked.replace(/(.{10})(.*)/, "$1/$2");
            masked = masked.replace(/(.{15})(.*)/, "$1-$2");
            return masked;
          }
        }
      }]);

      return CpfCnpjPipe;
    }();

    CpfCnpjPipe.ɵfac = function CpfCnpjPipe_Factory(t) {
      return new (t || CpfCnpjPipe)();
    };

    CpfCnpjPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
      name: "cpfcnpj",
      type: CpfCnpjPipe,
      pure: true
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CpfCnpjPipe, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
        args: [{
          name: 'cpfcnpj'
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/pipes/credit-card.pipe.ts":
  /*!*******************************************!*\
    !*** ./src/app/pipes/credit-card.pipe.ts ***!
    \*******************************************/

  /*! exports provided: CreditCardNumberPipe, CreditCardExpirationPipe */

  /***/
  function srcAppPipesCreditCardPipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CreditCardNumberPipe", function () {
      return CreditCardNumberPipe;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CreditCardExpirationPipe", function () {
      return CreditCardExpirationPipe;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var CreditCardNumberPipe =
    /*#__PURE__*/
    function () {
      function CreditCardNumberPipe() {
        _classCallCheck(this, CreditCardNumberPipe);
      }

      _createClass(CreditCardNumberPipe, [{
        key: "transform",
        value: function transform(number) {
          number = number.replace(/\./g, "");

          if (number.length >= 16) {
            number = number.substring(0, 16);
          }

          var masked = "";

          for (var i = 1; i < number.length + 1; i++) {
            masked = masked + number[i - 1];

            if (i % 4 == 0) {
              masked = masked + ".";
            }
          }

          if (masked.endsWith(".") && masked.length >= 20) {
            masked = masked.substring(0, masked.length - 1);
          }

          return masked;
        }
      }]);

      return CreditCardNumberPipe;
    }();

    CreditCardNumberPipe.ɵfac = function CreditCardNumberPipe_Factory(t) {
      return new (t || CreditCardNumberPipe)();
    };

    CreditCardNumberPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
      name: "ccnumber",
      type: CreditCardNumberPipe,
      pure: true
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CreditCardNumberPipe, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
        args: [{
          name: 'ccnumber'
        }]
      }], null, null);
    })();

    var CreditCardExpirationPipe =
    /*#__PURE__*/
    function () {
      function CreditCardExpirationPipe() {
        _classCallCheck(this, CreditCardExpirationPipe);
      }

      _createClass(CreditCardExpirationPipe, [{
        key: "transform",
        value: function transform(number) {
          number = number.replace(/\//g, "");

          if (number.length >= 4) {
            number = number.substring(0, 4);
          }

          var masked = "";

          for (var i = 1; i < number.length + 1; i++) {
            masked = masked + number[i - 1];

            if (i % 2 == 0) {
              masked = masked + "/";
            }
          }

          if (masked.endsWith("/") && masked.length >= 5) {
            masked = masked.substring(0, masked.length - 1);
          }

          return masked;
        }
      }]);

      return CreditCardExpirationPipe;
    }();

    CreditCardExpirationPipe.ɵfac = function CreditCardExpirationPipe_Factory(t) {
      return new (t || CreditCardExpirationPipe)();
    };

    CreditCardExpirationPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
      name: "ccexp",
      type: CreditCardExpirationPipe,
      pure: true
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CreditCardExpirationPipe, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
        args: [{
          name: 'ccexp'
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/services/auth.service.ts":
  /*!******************************************!*\
    !*** ./src/app/services/auth.service.ts ***!
    \******************************************/

  /*! exports provided: AuthService */

  /***/
  function srcAppServicesAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthService", function () {
      return AuthService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _commons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../commons */
    "./src/app/commons.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var AuthService =
    /*#__PURE__*/
    function () {
      function AuthService(http) {
        _classCallCheck(this, AuthService);

        this.http = http;
        this.loggedIn = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      }

      _createClass(AuthService, [{
        key: "getToken",
        value: function getToken(email, password) {
          var body = {
            "email": email,
            "senha": password
          };
          return this.http.post(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/login", body);
        }
      }, {
        key: "verifyToken",
        value: function verifyToken() {
          return this.http.get(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/login/verify");
        }
      }, {
        key: "resetarSenha",
        value: function resetarSenha(email) {
          return this.http.patch(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/login/esqueci-senha", {
            "email": email
          });
        }
      }, {
        key: "trocarSenha",
        value: function trocarSenha(novaSenha) {
          return this.http.patch(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/login", {
            "senha": novaSenha
          });
        }
      }]);

      return AuthService;
    }();

    AuthService.ɵfac = function AuthService_Factory(t) {
      return new (t || AuthService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
    };

    AuthService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: AuthService,
      factory: AuthService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      }, {
        loggedIn: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/services/convite.service.ts":
  /*!*********************************************!*\
    !*** ./src/app/services/convite.service.ts ***!
    \*********************************************/

  /*! exports provided: ConviteService */

  /***/
  function srcAppServicesConviteServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ConviteService", function () {
      return ConviteService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _commons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../commons */
    "./src/app/commons.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var ConviteService =
    /*#__PURE__*/
    function () {
      function ConviteService(http) {
        _classCallCheck(this, ConviteService);

        this.http = http;
      }

      _createClass(ConviteService, [{
        key: "enviar",
        value: function enviar(email) {
          return this.http.post(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/convite", {
            "email": email
          });
        }
      }, {
        key: "get",
        value: function get(codigo) {
          return this.http.get(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/convite/" + codigo);
        }
      }]);

      return ConviteService;
    }();

    ConviteService.ɵfac = function ConviteService_Factory(t) {
      return new (t || ConviteService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
    };

    ConviteService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: ConviteService,
      factory: ConviteService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ConviteService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/services/pagamento.service.ts":
  /*!***********************************************!*\
    !*** ./src/app/services/pagamento.service.ts ***!
    \***********************************************/

  /*! exports provided: PagamentoService */

  /***/
  function srcAppServicesPagamentoServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PagamentoService", function () {
      return PagamentoService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _commons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../commons */
    "./src/app/commons.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var PagamentoService =
    /*#__PURE__*/
    function () {
      function PagamentoService(http) {
        _classCallCheck(this, PagamentoService);

        this.http = http;
      }

      _createClass(PagamentoService, [{
        key: "getStatus",
        value: function getStatus(escritorioId) {
          return this.http.get(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/pagamento/status/" + escritorioId);
        }
      }, {
        key: "notificarFaltaPagamento",
        value: function notificarFaltaPagamento(notificar) {
          return this.http.patch(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/pagamento/notificacao", {
            "notificar": notificar
          });
        }
      }, {
        key: "criarAssinatura",
        value: function criarAssinatura(assinatura) {
          return this.http.post(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/pagamento/assinatura", assinatura);
        }
      }, {
        key: "cancelarAssinatura",
        value: function cancelarAssinatura() {
          return this.http["delete"](_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/pagamento/assinatura");
        }
      }, {
        key: "alterarCartao",
        value: function alterarCartao(assinatura) {
          return this.http.patch(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/pagamento/assinatura", assinatura);
        }
      }]);

      return PagamentoService;
    }();

    PagamentoService.ɵfac = function PagamentoService_Factory(t) {
      return new (t || PagamentoService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
    };

    PagamentoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: PagamentoService,
      factory: PagamentoService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PagamentoService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/services/termos.service.ts":
  /*!********************************************!*\
    !*** ./src/app/services/termos.service.ts ***!
    \********************************************/

  /*! exports provided: TermosService */

  /***/
  function srcAppServicesTermosServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TermosService", function () {
      return TermosService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _commons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../commons */
    "./src/app/commons.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var TermosService =
    /*#__PURE__*/
    function () {
      function TermosService(http) {
        _classCallCheck(this, TermosService);

        this.http = http;
      }

      _createClass(TermosService, [{
        key: "insert",
        value: function insert(termo, emails) {
          var body = {
            "termo": termo,
            "emails": emails
          };
          return this.http.post(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/termo", body);
        }
      }, {
        key: "listar",
        value: function listar() {
          return this.http.get(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/termo");
        }
      }, {
        key: "delete",
        value: function _delete(termoId) {
          return this.http["delete"](_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/termo/" + termoId);
        }
      }, {
        key: "pararAcompanhamentoAdm",
        value: function pararAcompanhamentoAdm(termoId, usuarios) {
          var body = {
            "id": termoId,
            "usuarios": usuarios
          };
          return this.http.patch(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/termo/parar-acompanhamento", body);
        }
      }, {
        key: "pararAcompanhamentoUser",
        value: function pararAcompanhamentoUser(termoId) {
          return this.http["delete"](_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/termo/parar-acompanhamento/" + termoId);
        }
      }]);

      return TermosService;
    }();

    TermosService.ɵfac = function TermosService_Factory(t) {
      return new (t || TermosService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
    };

    TermosService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: TermosService,
      factory: TermosService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TermosService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/services/usuario.service.ts":
  /*!*********************************************!*\
    !*** ./src/app/services/usuario.service.ts ***!
    \*********************************************/

  /*! exports provided: UsuarioService */

  /***/
  function srcAppServicesUsuarioServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UsuarioService", function () {
      return UsuarioService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _commons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../commons */
    "./src/app/commons.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var UsuarioService =
    /*#__PURE__*/
    function () {
      function UsuarioService(http) {
        _classCallCheck(this, UsuarioService);

        this.http = http;
      }

      _createClass(UsuarioService, [{
        key: "getAll",
        value: function getAll() {
          return this.http.get(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/usuario");
        }
      }, {
        key: "delete",
        value: function _delete(id) {
          return this.http["delete"](_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/usuario/" + id);
        }
      }, {
        key: "cadastrar",
        value: function cadastrar(convite, nome, senha) {
          var body = {
            "codigo_convite": convite,
            "nome": nome,
            "senha": senha
          };
          return this.http.post(_commons__WEBPACK_IMPORTED_MODULE_1__["SERVER"] + "/usuario", body);
        }
      }]);

      return UsuarioService;
    }();

    UsuarioService.ɵfac = function UsuarioService_Factory(t) {
      return new (t || UsuarioService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
    };

    UsuarioService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: UsuarioService,
      factory: UsuarioService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UsuarioService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    }); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
    }

    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
      return console.error(err);
    });
    /***/

  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! /root/legaltracker/webapp/src/main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map