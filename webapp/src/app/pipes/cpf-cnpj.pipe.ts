import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'cpfcnpj'
})
export class CpfCnpjPipe implements PipeTransform {
	transform(txt: string): string {
		txt = txt.replace(/[^0-9]/g,"")
		let masked = ""

		if (txt.length <= 11){
			for(let i=1; i < txt.length+1; i++){
				masked = masked + txt[i-1]
				if (i % 3 == 0){
					masked = masked + "."
				}
			}
			let count = (masked.match(/\./g) || []).length;
			if(count == 3){
				let pos = masked.lastIndexOf('.');
				masked = masked.substring(0,pos) + '-' + masked.substring(pos+1)
			}
			return masked
		} else {
			masked = txt.replace(/(.{2})(.*)/, "$1.$2")
			masked = masked.replace(/(.{6})(.*)/, "$1.$2")
			masked = masked.replace(/(.{10})(.*)/, "$1/$2")
			masked = masked.replace(/(.{15})(.*)/, "$1-$2")
	        return masked
		}
	}
}
