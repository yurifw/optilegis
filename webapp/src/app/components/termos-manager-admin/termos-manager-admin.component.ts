import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TermosService } from '../../services/termos.service'
import Swal from 'sweetalert2'	//https://sweetalert2.github.io/	https://stackoverflow.com/questions/41684114/angular-2-easy-way-to-make-a-confirmation-dialog

@Component({
	selector: 'app-termos-manager-admin',
	templateUrl: './termos-manager-admin.component.html',
	styleUrls: ['./termos-manager-admin.component.css']
})
export class TermosManagerAdminComponent implements OnInit {
	novoTermo = ""
	novoEmail = ""
	emails = []
	termosSeguidos = []
	@ViewChild('termosContainer') termosContainer: ElementRef;
	termoHtmlTemplate = ""
	idSeguindoExibido = null
	constructor(private toastr: ToastrService, private termoService: TermosService) { }

	ngOnInit(): void {
		this.buscarTermosAtuais()
	}
	adicionarEmail(){
		if(this.novoEmail.length < 3){
			this.toastr.error("Email inválido")
			return
		}
		if(!this.novoEmail.includes("@")){
			this.toastr.error("Email inválido")
			return
		}

		this.emails.push(this.novoEmail)
		this.novoEmail = ''

	}
	removerEmail(email){
		const index = this.emails.indexOf(email);
		if (index !== -1) {
			this.emails.splice(index, 1);
		}
	}

	salvarTermos(){
		this.termoService.insert(this.novoTermo, this.emails).subscribe(resp => {
			if (resp.success){
				this.emails = []
				this.novoTermo = ''
				this.buscarTermosAtuais()
			}
		})
	}

	buscarTermosAtuais(){
		this.termoService.listar().subscribe((resp)=>{
			if(resp.success){
				this.termosSeguidos = resp.data
			}
		})
	}

	exibirSeguidores(id){
		if(id == this.idSeguindoExibido){
			this.idSeguindoExibido = null
		} else {
			this.idSeguindoExibido = id
		}
	}

	removerSeguidor(seguidor, termo){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'O email '+seguidor.email+' não receberá mais alertas para o termo "'+termo.termo_texto+'"',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
					this.termoService.pararAcompanhamentoAdm(termo.termo_id, [{"id":seguidor.id}]).subscribe((resp) => {
					if(resp.success){
						this.buscarTermosAtuais()
					}
				})
			}
		})
	}

	deletarTermo(termo){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'O termo "'+termo.termo_texto+'" será deletado e nenhum usuário receberá notificações sobre este termo',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
					this.termoService.delete(termo.termo_id).subscribe((resp) => {
					if(resp.success){
						this.buscarTermosAtuais()
					}
				})
			}
		})
	}
}
