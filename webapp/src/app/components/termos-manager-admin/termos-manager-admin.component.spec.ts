import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermosManagerAdminComponent } from './termos-manager-admin.component';

describe('TermosManagerAdminComponent', () => {
  let component: TermosManagerAdminComponent;
  let fixture: ComponentFixture<TermosManagerAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermosManagerAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermosManagerAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
