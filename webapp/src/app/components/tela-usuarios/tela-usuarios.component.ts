import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service'
import { TermosService } from '../../services/termos.service'
import { ConviteService } from '../../services/convite.service'
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'	//https://sweetalert2.github.io/	https://stackoverflow.com/questions/41684114/angular-2-easy-way-to-make-a-confirmation-dialog

@Component({
	selector: 'app-tela-usuarios',
	templateUrl: './tela-usuarios.component.html',
	styleUrls: ['./tela-usuarios.component.css']
})
export class TelaUsuariosComponent implements OnInit {
	novoUsuario = ""
	usuarios = []
	idUsuarioDetalhado = 0

	constructor(
		private usuarioService: UsuarioService,
		private termoService: TermosService,
		private conviteService: ConviteService,
		private toastr: ToastrService
	) { }

	ngOnInit(): void {
		this.atualizarUsuarios()
	}

	convidar(){
		if(this.novoUsuario.length < 3){
			this.toastr.error("Email inválido")
			return
		}
		if(!this.novoUsuario.includes("@")){
			this.toastr.error("Email inválido")
			return
		}
		this.conviteService.enviar(this.novoUsuario).subscribe((resp) => {
			if(resp.success){
				this.novoUsuario = ""
				this.atualizarUsuarios()
			}
		})
	}

	atualizarUsuarios(){
		this.usuarioService.getAll().subscribe((resp) => {
			if(resp.success){
				this.usuarios = resp.data
				console.log(this.usuarios)
			}
		})
	}

	delete(user){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'O usuario com email '+user.email+' será deletado. Isso significa que ele não receberá mais nenhum alerta e perderá acesso ao sistema (embora você possa enviar um novo convite)',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.usuarioService.delete(user.id).subscribe((resp) => {
					if(resp.success){
						this.atualizarUsuarios()
					}
				})
			}
		})


	}

	abandonaTermo(termo, usuario){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'O email '+usuario.email+' não receberá mais notificações para o termo "'+termo.termo+'"',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.termoService.pararAcompanhamentoAdm(termo.id, [{"id":usuario.id}]).subscribe((resp) => {
					if (resp.success){
						this.atualizarUsuarios()
					}
				})
			}
		})

	}

	detalharUsuario(id){
		if(id == this.idUsuarioDetalhado){
			this.idUsuarioDetalhado = 0
		} else {
			this.idUsuarioDetalhado = id
		}
	}

}
