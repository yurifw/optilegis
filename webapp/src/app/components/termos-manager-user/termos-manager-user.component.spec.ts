import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermosManagerUserComponent } from './termos-manager-user.component';

describe('TermosManagerUserComponent', () => {
  let component: TermosManagerUserComponent;
  let fixture: ComponentFixture<TermosManagerUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermosManagerUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermosManagerUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
