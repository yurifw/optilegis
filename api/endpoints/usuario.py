from flask import Flask, make_response
from flask import request
import bcrypt
import random
import string
import pymysql
from .send_email import send_email, TEMPLATE
from . import database as db
from .auth import requires_admin, decode_token

@requires_admin(True)
def convidar():
	conn, cursor = db.ready()
	try:
		email = request.json['email']
		token = decode_token(request.headers['Authorization'])['payload']

		cursor.execute("select * from usuario where email = %s and senha is not null", [email])
		u = cursor.fetchone()
		if u is not None:
			return make_response({"msg":"Email já cadastrado como usuário","data":None, "success":False})

		codigo = ''.join([random.choice(string.printable[0:36]) for a in range(20)])
		sql = "insert into convite (codigo, convidado_por, email) values (%s,%s,%s) on duplicate key update codigo=%s, convidado_por=%s"
		cursor.execute(sql, [codigo, token['user_id'], email, codigo, token['user_id']])
		corpo = TEMPLATE['convite'].format(codigo=codigo)
		send_email([email], "Convite para cadastro no Adm DO", corpo)
		conn.commit()
		return make_response({"msg":"Convite enviado com sucesso!","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()

def list_convite(codigo):
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from convite where codigo = %s", [codigo])
		convite = cursor.fetchone()
		return make_response({"msg":None,"data":convite, "success":True})
	finally:
		cursor.close()
		conn.close()

def insert():
	conn, cursor = db.ready()
	try:
		convite = request.json['codigo_convite']
		nome = request.json['nome']
		senha = request.json['senha']

		sql = "select u.*, c.email as email_convite from usuario u, convite c where c.codigo = %s and u.id = c.convidado_por"
		cursor.execute(sql, [convite])
		emissor = cursor.fetchone()
		if emissor is None:
			return make_response({"msg":"Convite não encontrado","data":None, "success":False})
		escritorio_id = emissor['escritorio_id']
		email = emissor['email_convite']

		hashed = bcrypt.hashpw(senha.encode(), bcrypt.gensalt())
		sql = """
			insert into usuario (nome, email, senha, escritorio_id, admin) values (%s, %s, %s, %s, 0)
			on duplicate key update nome = %s, senha =%s
		"""
		cursor.execute(sql, [nome, email, hashed, escritorio_id, nome, hashed])
		cursor.execute("delete from convite where codigo =%s", [convite])
		conn.commit()
		return make_response({"msg":"Cadastro realizado com sucesso!","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()

@requires_admin(True)
def list_all():
	conn, cursor = db.ready()
	try:
		escritorio_id = decode_token(request.headers['Authorization'])['payload']['escritorio_id']
		cursor.execute("select id, nome, email, senha is not null as cadastrado from usuario where escritorio_id = %s order by email", [escritorio_id])
		usuarios = cursor.fetchall()
		for usuario in usuarios:
			sql = "select t.termo, t.id from usuario_termos ut, termo t where ut.usuario_id = %s and ut.termo_id = t.id"
			cursor.execute(sql, [usuario['id']])
			usuario['termos'] = cursor.fetchall()
			usuario['cadastrado'] = usuario['cadastrado'] == 1
		return make_response({"msg":None, "data":usuarios, "success":True})
	finally:
		cursor.close()
		conn.close()

@requires_admin(True)
def delete(id):
	conn, cursor = db.ready()
	try:
		cursor.execute("select admin from usuario where id =%s", [id])
		u = cursor.fetchone()
		if u['admin'] == 1:
			return make_response({"msg":"Não é possível deletar um usuário administrador","data":None, "success":False})
		cursor.execute("delete from usuario where id = %s", [id])
		conn.commit()
		return make_response({"msg":"Usuário deletado","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()

@requires_admin(False)
def trocar_senha():
	conn, cursor = db.ready()
	try:
		senha = request.json['senha']
		hashed = bcrypt.hashpw(senha.encode(), bcrypt.gensalt())
		token = decode_token(request.headers['Authorization'])['payload']
		cursor.execute("update usuario set senha = %s where id = %s", [hashed, token['user_id']])
		conn.commit()
		return make_response({"msg":"Senha alterada com sucesso","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()
