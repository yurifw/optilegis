from flask import Flask, make_response
from flask import request
import jwt
from jwt.exceptions import ExpiredSignatureError
import bcrypt
from datetime import datetime, timedelta
from functools import wraps
import string
import random
import smtplib
from . import database as db
from .send_email import send_email, TEMPLATE

def generate_token(user_id, email, nome, admin, escritorio_id, quick_expire = False):
	private_key = None
	with open('../keys/jwtRS256.key', 'r') as priv:
		private_key = priv.read().encode()
	if quick_expire:
		expiration = datetime.utcnow() + timedelta(minutes=15)
	else:
		expiration = datetime.utcnow() + timedelta(days=30)
	payload = {"user_id":user_id, "email":email,"nome":nome, "escritorio_id":escritorio_id, "admin":admin == 1, "exp":expiration}
	token = jwt.encode(payload, private_key, algorithm='RS256')
	return token.decode()

def get_token():
	conn, cursor = db.ready()
	try:
		email = request.json['email']
		senha = request.json['senha']

		cursor.execute('select * from usuario where email = %s and senha is not null', [email])
		usuario = cursor.fetchone()

		if usuario is None:
			return make_response({"msg":'Erro de autenticação', "success":False, "data":None})

		hash = usuario['senha']

		if bcrypt.checkpw(senha.encode(), hash.encode()):
			jwt = generate_token(usuario['id'], email, usuario['nome'], usuario['admin'], usuario['escritorio_id'])
			return make_response({"msg":None, "success":True, "data":jwt})
		else:
			return make_response({"msg":'Erro de autenticação', "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

def decode_token(token):
	conn, cursor = db.ready()
	try:
		public_key = None
		payload = None
		valid = False
		with open('../keys/public.key', 'r') as pub:
			public_key = pub.read().encode()
		payload = jwt.decode(token, public_key, algorithms=['RS256'], verify=True)
		valid = True
		cursor.execute("select count(*) as qtd from usuario where id = %s and senha is not null", [payload['user_id']])
		result = cursor.fetchone()
		if result['qtd'] == 0:
			valid = False
			payload = None
	except:
		pass
	finally:
		cursor.close()
		conn.close()

	decoded_token = {"isValid":valid, "payload":payload}
	return decoded_token

def verify_token():
	token = None
	try:
		token = request.headers['Authorization']
	except KeyError:
		return make_response({"msg":'Envie a chave no header "Authorization"', "success":False, "data":None})

	return make_response({"msg":None, "success":True, "data":decode_token(token)})

def requires_admin(yes):  # use como decorator, yes eh o boolean, true se precisar de admin, false se nao precisar de admin
	def decorator(func):
		@wraps(func)
		def decorated_func(*args, **kws):
			token = None
			try:
				token = request.headers['Authorization']
			except KeyError:
				return make_response({"msg":'Envie a chave no header "Authorization"', "success":False, "data":None})
			decoded = decode_token(token)
			if not decoded['isValid']:
				return make_response({"msg":'Ação não permitida', "success":False, "data":None})

			conn, cursor = db.ready()
			try:
				cursor.execute("select * from usuario where id = %s", [decoded['payload']['user_id']])
				if cursor.fetchone() is None:
					return make_response({"msg":'Ação não permitida', "success":False, "data":None})
				else:
					if yes and not decoded['payload']['admin']:
						return make_response({"msg":'Ação não permitida', "success":False, "data":None})
				return func(*args, **kws)
			finally:
				cursor.close()
				conn.close()
		return decorated_func
	return decorator

def esqueci_senha():
	conn, cursor = db.ready()
	try:
		email = request.json['email']
		cursor.execute("select * from usuario where email = %s and senha is not null", [email])
		usuario = cursor.fetchone()
		if usuario is None:
			return make_response({"msg":"Email não cadastrado","data":None, "success":False})

		jwt = generate_token(usuario['id'], email, usuario['nome'], usuario['admin'], usuario['escritorio_id'], True)
		corpo = TEMPLATE['esqueci-senha'].format(codigo = jwt)
		send_email([email], "Recuperação da senha Adm DO", corpo)
		return make_response({"msg":"Email de recuperação de senha enviado","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()
