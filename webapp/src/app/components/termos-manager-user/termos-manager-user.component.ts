import { Component, OnInit} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TermosService } from '../../services/termos.service'
import Swal from 'sweetalert2'	//https://sweetalert2.github.io/	https://stackoverflow.com/questions/41684114/angular-2-easy-way-to-make-a-confirmation-dialog


@Component({
	selector: 'app-termos-manager-user',
	templateUrl: './termos-manager-user.component.html',
	styleUrls: ['./termos-manager-user.component.css']
})
export class TermosManagerUserComponent implements OnInit {
	novoTermo = ''
	termos = []
	constructor(private toastr: ToastrService, private termoService: TermosService) { }

	ngOnInit(): void {
		this.atualizarTermos()
	}

	atualizarTermos(){
		this.termoService.listar().subscribe(resp => {
			if(resp.success){
				this.termos = resp.data
			}
		})
	}

	acompanhar(){
		this.termoService.insert(this.novoTermo, null).subscribe((resp)=>{
			if (resp.success){
				this.atualizarTermos()
				this.novoTermo = ''
			}
		})
	}

	abandonar(id){
		this.termoService.pararAcompanhamentoUser(id).subscribe((resp) =>{
			if (resp.success){
				this.atualizarTermos()
			}
		})
	}

}
