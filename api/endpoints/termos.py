from flask import Flask, make_response
from flask import request
import pymysql
from . import database as db
from .auth import decode_token, requires_admin

@requires_admin(False)
def insert():
	"""
		{termo:"Artigo 107", emails:["email1@email.com", "email2@email.com"]}
		se o usuario for admin:
		para cada email recebido, verifica na tabela de usuarios se existe um usuario com o email cadastrado.
		Se existe, este usuario passa a seguir o email.
		se nao existe, criar um registro na tabela de usuario:
		os campos nome e senha devem ser nulos, o campo de email é o email recebido e o escritorio deve ser o mesmo escritorio da JWT, o campo admin SEMPRE deve ser falso
		o novo registro de usuario deve seguir o termo
		se nao for admin o proprio usuario passa a seguir o termo e ignora lista de emails
	"""
	token = decode_token(request.headers['Authorization'])['payload']

	conn, cursor = db.ready()
	try:
		termo = request.json
		termo['termo'] = termo['termo'].lower()
		termo_id = None
		try:
			cursor.execute("insert into termo (termo, escritorio_id) values (%s, %s)", [termo['termo'],token['escritorio_id']])
			termo_id = cursor.lastrowid
		except pymysql.err.IntegrityError:
			print("termo ja cadastrado")
			cursor.execute("select * from termo where termo = %s and escritorio_id=%s", [termo['termo'], token['escritorio_id']])
			termo_id =cursor.fetchone()['id']

		if token['admin']:
			for email in termo['emails']:
				cursor.execute("select * from usuario where email = %s", [email])
				usuario = cursor.fetchone()
				if usuario is None:
					cursor.execute("insert into usuario (email, escritorio_id, admin) values(%s, %s, 0)", [email, token['escritorio_id']])
					usuario = {'id':cursor.lastrowid}
				try:
					cursor.execute("insert into usuario_termos (usuario_id, termo_id) values (%s,%s)", [usuario['id'],termo_id])
				except pymysql.err.IntegrityError:
					pass

		else:
			try:
				cursor.execute("insert into usuario_termos (usuario_id, termo_id) values (%s,%s)", [token['user_id'],termo_id])
			except pymysql.err.IntegrityError:
				pass
		conn.commit()
		return make_response({"msg":"Termo cadastrado com sucesso!","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()

@requires_admin(False)
def list():
	conn, cursor = db.ready()
	try:
		token = decode_token(request.headers['Authorization'])['payload']
		data = []
		if(token['admin']):
			cursor.execute("select * from termo where escritorio_id = %s order by termo", [token['escritorio_id']])
			termos = cursor.fetchall()
			for termo in termos:
				row = {"termo_id":termo['id'], "termo_texto":termo['termo'], "seguindo":[]}
				cursor.execute("select u.nome, u.id, u.email, u.escritorio_id, u.admin from usuario u, usuario_termos t where t.termo_id=%s and t.usuario_id = u.id order by u.email", [termo['id']])
				row['seguindo'].extend(cursor.fetchall())
				data.append(row)
		else:
			cursor.execute("select t.id, t.termo as termo_texto from termo t, usuario_termos ut where ut.usuario_id = %s and ut.termo_id = t.id order by termo", [token['user_id']])
			data = cursor.fetchall()

		return make_response({"msg":None,"data":data, "success":True})
	finally:
		cursor.close()
		conn.close()

@requires_admin(False)
def parar_acompanhamento(termo_id):
	conn, cursor = db.ready()
	try:
		token = decode_token(request.headers['Authorization'])['payload']
		cursor.execute("delete from usuario_termos where termo_id = %s and usuario_id = %s", [termo_id, token['user_id']])
		conn.commit()
		return make_response({"msg":"Termo abandonado com sucesso","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()

@requires_admin(True)
def parar_acompanhamento_admin():
	conn, cursor = db.ready()
	try:
		token = decode_token(request.headers['Authorization'])['payload']
		termo = request.json
		termo_id = None
		cursor.execute("select * from termo where id = %s and escritorio_id = %s", [termo['id'],token['escritorio_id']])
		t = cursor.fetchone()
		if t is None:
			return make_response({"msg":"Termo não encontrado","data":none, "success":False})
		termo_id = t['id']
		for usuario in termo['usuarios']:
			cursor.execute("delete from usuario_termos where termo_id = %s and usuario_id = %s", [termo_id, usuario['id']])

		conn.commit()
		return make_response({"msg":"Termo abandonado com sucesso","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()

@requires_admin(True)
def delete(termo_id):
	conn, cursor = db.ready()
	try:
		token = decode_token(request.headers['Authorization'])['payload']
		cursor.execute("delete from termo where id = %s and escritorio_id = %s", [termo_id, token['escritorio_id']])
		conn.commit()
		return make_response({"msg":"Termo deletado com sucesso","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()
