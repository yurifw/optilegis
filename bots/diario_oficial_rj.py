# -*- coding: utf-8 -*-
# C:\Users\Pichau\AppData\Local\Programs\Python\Python37\python.exe sketch.py
import fitz
from bs4 import BeautifulSoup
import requests
import re
from datetime import date, timedelta, datetime
import base64
import re
import utils
import sys
import os
import codecs


script_path = sys.argv[0].replace('run.py','')
do_path = '%sdo.pdf' % (script_path)
do_destacado_path = '%sdodestacado.pdf' % (script_path)

def get_pdf_key():
	data = date.today()
	# data = data - timedelta(days = 1)
	data = data.strftime("%Y%m%d")
	encoded64 = base64.b64encode(data.encode())
	encoded64 =str(encoded64, "utf-8")
	link = "http://www.ioerj.com.br/portal/modules/conteudoonline/do_seleciona_edicao.php?data=%s" % encoded64

	page = requests.get(link)
	soup = BeautifulSoup(page.text, 'lxml')

	key_link = ""
	for anchor in soup.findAll('a'):
		print(anchor.text)
		if "Poder Executivo" in anchor.text:
			key_link = "http://www.ioerj.com.br/portal/modules/conteudoonline/%s" % (anchor['href'])
	if key_link == "":
		print("nao foi possivel encontrar link da chave na url:\n%s" % link)
		raise Exception("nao foi possivel encontrar link da chave na url:\n%s" % link)

	page = requests.get(key_link).text
	regex = 'text/javascript">var\spd\s*=\s*"(.*?)"'
	m = re.search(regex, page)
	if m is None:
		print("erro, enviar email")
	key = m.group(1)
	key_parts = key.split("-")
	key_parts[1] = key_parts[1][:3]+"P"+key_parts[1][3:]
	return "-".join(key_parts)

def download_pdf():
	key = get_pdf_key()
	download_link = "http://www.ioerj.com.br/portal/modules/conteudoonline/mostra_edicao.php?k=%s" % key

	doc = requests.get(download_link).content

	with open(do_path, 'wb') as f:
		f.write(doc)

def search_pdf(termos):
	doc = fitz.open(do_path)
	result = {}
	for termo in termos:
		result[termo] = []
		for pagina in doc:
			text_instances = pagina.searchFor(termo)

			if len(text_instances)>0:
				result[termo].append(pagina.number+1)

			for inst in text_instances:
			    highlight = pagina.addHighlightAnnot(inst)

	doc.save(do_destacado_path, garbage=4, deflate=False, clean=False)
	return result

def read_termos():
	with open("termos","r", encoding="utf-8") as f:
		return f.read().splitlines()

def salva_do(cursor):
	hoje = datetime.now().strftime("%d/%m/%Y")
	cursor.execute("select * from diario_oficial where DATE_FORMAT(data_publicacao, '%%d/%%m/%%Y') = %s", [hoje])
	diarios = cursor.fetchall()
	if len(diarios) > 0:
		return
	with open(do_path, 'rb') as do:
		blob = do.read()
		cursor.execute("insert into diario_oficial (data_publicacao, arquivo) values (str_to_date(%s,'%%d/%%m/%%Y'), %s)", [hoje, blob])

def limpa_pdfs():
	os.remove(do_path)
	os.remove(do_destacado_path)

#
# conn = None
# cursor = None
# try:
# 	conn, cursor = utils.connect_db()
# 	download_pdf()
# 	salva_do(cursor)
# 	termos = utils.get_termo_list(cursor)
# 	for email in termos:
# 		if 'yuri' not in email:
# 			continue
# 		resultado = search_pdf(termos[email])
# 		# email_corpo = print_result(resultado)
# 		utils.envia_mail([email], 'Resultado', "encontrado", ['dodestacado.pdf'])
# finally:
# 	cursor.close()
# 	conn.close()
