import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router"

@Component({
	selector: 'app-tela-login',
	templateUrl: './tela-login.component.html',
	styleUrls: ['./tela-login.component.css']
})
export class TelaLoginComponent implements OnInit {
	email = ""
	senha = ""
	isLoggedIn = false;

	constructor(
		private auth: AuthService,
		private toastr: ToastrService,
		private router: Router,
		private activatedRoute: ActivatedRoute
	) { }

	ngOnInit(): void {
		let jwt = this.activatedRoute.snapshot.params['jwt'];
		if(jwt){
			localStorage.setItem('jwt',jwt)
		}
		console.log(jwt)
		this.auth.verifyToken().subscribe((resp)=>{
			this.isLoggedIn = resp.data.isValid;
			if(this.isLoggedIn){
				this.auth.loggedIn.emit(true)
				this.router.navigate(['termos'])
			}
		})
	}

	login(){
		this.auth.getToken(this.email, this.senha).subscribe((resp) => {
			if(resp.success){
				localStorage.setItem("jwt",resp.data)
				this.router.navigate(['termos'])
				this.auth.loggedIn.emit(true)
			}
		})
	}

	esqueciSenha(){
		if(this.email){
			this.auth.resetarSenha(this.email).subscribe()
		} else {
			this.toastr.error("Digite seu email")
		}
	}

}
