import { Injectable } from '@angular/core';
import { SERVER, Resposta } from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class ConviteService {

	constructor(private http: HttpClient) { }

	enviar(email){
		return this.http.post<Resposta>(SERVER+"/convite", {"email":email})
	}

	get(codigo){
		return this.http.get<Resposta>(SERVER+"/convite/"+codigo)
	}
}
