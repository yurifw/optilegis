import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'ccnumber'
})
export class CreditCardNumberPipe implements PipeTransform {
	transform(number: string): string {
		number = number.replace(/\./g,"")
		if (number.length >= 16){
			number = number.substring(0, 16)
		}
		let masked = ""
		for(let i=1; i < number.length+1; i++){
			masked = masked + number[i-1]
			if (i % 4 == 0){
				masked = masked + "."
			}
		}
		if (masked.endsWith(".") && masked.length >= 20){
			masked = masked.substring(0, masked.length - 1);
		}
		return masked
	}
}

@Pipe({
	name: 'ccexp'
})
export class CreditCardExpirationPipe implements PipeTransform {
	transform(number: string): string {
		number = number.replace(/\//g,"")
		if (number.length >= 4){
			number = number.substring(0, 4)
		}
		let masked = ""
		for(let i=1; i < number.length+1; i++){
			masked = masked + number[i-1]
			if (i % 2 == 0){
				masked = masked + "/"
			}
		}
		if (masked.endsWith("/") && masked.length >= 5){
			masked = masked.substring(0, masked.length - 1);
		}
		return masked
	}
}
