import os

from flask import Flask
from flask import render_template
from flask_cors import CORS
from flask import request

import endpoints.termos as termos
import endpoints.auth as auth
import endpoints.usuario as usuario
import endpoints.pagamento as pagamento

app = Flask(__name__, static_folder='public/')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.debug = True
CORS(app)

app.add_url_rule('/api/login', 'a_p', auth.get_token, methods=['POST'])
app.add_url_rule('/api/login', 'a_s', usuario.trocar_senha, methods=['PATCH'])
app.add_url_rule('/api/login/verify', 'a_g', auth.verify_token, methods=['GET'])
app.add_url_rule('/api/login/esqueci-senha', 's_e', auth.esqueci_senha, methods=['PATCH'])
app.add_url_rule('/api/convite', 'c_g', usuario.convidar, methods=['POST'])
app.add_url_rule('/api/convite/<string:codigo>', 'c_l', usuario.list_convite, methods=['GET'])
app.add_url_rule('/api/termo', 't_p', termos.insert, methods=['POST'])
app.add_url_rule('/api/termo', 't_l', termos.list, methods=['GET'])
app.add_url_rule('/api/termo/parar-acompanhamento/<int:termo_id>', 't_d', termos.parar_acompanhamento, methods=['DELETE'])
app.add_url_rule('/api/termo/<int:termo_id>', 't_did', termos.delete, methods=['DELETE'])
app.add_url_rule('/api/termo/parar-acompanhamento', 't_ad', termos.parar_acompanhamento_admin, methods=['PATCH'])
app.add_url_rule('/api/usuario', 'u_i', usuario.insert, methods=['POST'])
app.add_url_rule('/api/usuario', 'u_ia', usuario.list_all, methods=['GET'])
app.add_url_rule('/api/usuario/<int:id>', 'u_d', usuario.delete, methods=['DELETE'])
app.add_url_rule('/api/pagamento/status/<int:escritorio_id>', 'p_s', pagamento.get_status, methods=['GET'])
app.add_url_rule('/api/pagamento/notificacao', 'pn', pagamento.notificar_falta_pagamento, methods=['PATCH'])
app.add_url_rule('/api/pagamento/assinatura', 'pa', pagamento.criar_assinatura, methods=['POST'])
app.add_url_rule('/api/pagamento/assinatura', 'pp', pagamento.trocar_cartao, methods=['PATCH'])
app.add_url_rule('/api/pagamento/assinatura', 'pd', pagamento.cancelar_assinatura, methods=['DELETE'])



# sends 404 to index so angular can handle them
@app.errorhandler(404)
def not_found(e):
	requested_file = request.path[1:] if len(request.path[1:]) > 0 else "index.html"
	if "." not in requested_file or (requested_file.startswith('login') and len(requested_file) > 40):
		requested_file = "index.html"
	return app.send_static_file(requested_file)
