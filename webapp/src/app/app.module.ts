import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';

import { CreditCardNumberPipe } from './pipes/credit-card.pipe';
import { CreditCardExpirationPipe } from './pipes/credit-card.pipe';
import { CpfCnpjPipe } from './pipes/cpf-cnpj.pipe';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TelaLoginComponent } from './components/tela-login/tela-login.component';
import { ApiInterceptor } from './api.interceptor';
import { TelaTermosComponent } from './components/tela-termos/tela-termos.component';
import { TermosManagerAdminComponent } from './components/termos-manager-admin/termos-manager-admin.component';
import { TermosManagerUserComponent } from './components/termos-manager-user/termos-manager-user.component';
import { TelaUsuariosComponent } from './components/tela-usuarios/tela-usuarios.component';
import { TelaConviteComponent } from './components/tela-convite/tela-convite.component';
import { TelaConfigComponent } from './components/tela-config/tela-config.component';
import { TelaPagamentoComponent } from './components/tela-pagamento/tela-pagamento.component';

@NgModule({
	declarations: [
		AppComponent,
		TelaLoginComponent,
		TelaTermosComponent,
		TermosManagerAdminComponent,
		TermosManagerUserComponent,
		TelaUsuariosComponent,
		TelaConviteComponent,
		TelaConfigComponent,
		TelaPagamentoComponent,
		CreditCardNumberPipe,
		CreditCardExpirationPipe,
		CpfCnpjPipe
	],
	imports: [
		FormsModule,
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		BrowserAnimationsModule,
		ToastrModule.forRoot({ positionClass: 'toast-bottom-center', preventDuplicates: true }),
	],
	providers: [
		{ provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true }
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
