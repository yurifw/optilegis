import pymysql
from datetime import datetime, timedelta
import requests
from requests.auth import HTTPBasicAuth
import json
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os.path import basename

dias_gratis = 14

def connect_db():
	MYSQL_HOST = 'localhost'
	MYSQL_USER = 'legaluser'
	MYSQL_PASSWORD = 'zQqB0nrIDokxEcS14yTD'
	MYSQL_DB = 'legaltracker'
	MYSQL_PORT = 3306

	conn = pymysql.connect(
        host=MYSQL_HOST,
        db=MYSQL_DB,
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        cursorclass=pymysql.cursors.DictCursor
    )
	conn.autocommit(True)
	cursor = conn.cursor()
	return (conn, cursor)

def log(text):
	ts = datetime.now().strftime("[%d/%m/%y %H:%M:%S]")
	print("%s %s" % (ts, text))

def envia_mail(para, assunto, corpo, anexos=None):
	"""
		para: list - list com enderecos de email que serao os destinatarios
		assunto: str - aparecera como assunto
		corpo: str - texto que sera o corpo do email
		anexos: list - lista de caminhos de arquivos para serem anexados no email
	"""
	# CONFIG #
	# email_server = "br46.hostgator.com.br"
	# email_port = "465"
	# email_user = "noreply@polarium.com.br"
	# email_pwd = "@3Mn2Jfp#n_d"
	email_server = "mail.admdo.com.br"
	email_port = "465"
	email_user = "noreply@admdo.com.br"
	email_pwd = "HUQY@05nlLY"
	####################################

	msg = MIMEMultipart()
	msg['From'] = email_user
	msg['To'] = ','.join(para)
	msg['Subject'] = assunto

	msg.attach(MIMEText(corpo, 'html'))

	for f in anexos or []:
		with open(f, "rb") as fil:
			part = MIMEApplication(
				fil.read(),
				Name=basename(f)
			)
		# After the file is closed
		part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
		msg.attach(part)

	smtp = smtplib.SMTP_SSL(email_server, email_port)
	# smtp.set_debuglevel(True)
	smtp.login(email_user, email_pwd)
	smtp.sendmail(email_user, para, msg.as_string())
	smtp.close()

def get_termo_list(cursor):
	"""Busca todos os escritorios cadastrados e verifica se o escritorio efetuou
	os pagamentos, se o pagamento está em dia, monta uma lista dos emails com os
	termos que devem ser procurados. Se o pagamento não está em dia verifica se
	o escritorio deseja receber aviso sobre pagamento e envia o aviso se for o
	caso.
    """

	cursor.execute("select * from escritorio")
	escritorios = cursor.fetchall()

	termos = {}
	for escritorio in escritorios:
		cursor.execute("select * from usuario where escritorio_id = %s",[escritorio['id']])
		usuarios = cursor.fetchall()
		for usuario in usuarios:
			termos[usuario['email']] = []
			cursor.execute("select t.termo from termo t, usuario_termos ut, usuario u where u.id = %s and ut.usuario_id = u.id and t.id = ut.termo_id", [usuario['id']])
			result = cursor.fetchall()
			for r in result:
				termos[usuario['email']].append(r['termo'])

	return termos
