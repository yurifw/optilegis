export const SERVER = "https://admdo.com.br/api";

export interface Resposta {
	msg: string;
	success: boolean;
	data: any;
}
