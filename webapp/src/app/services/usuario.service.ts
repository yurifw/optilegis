import { Injectable, Output } from '@angular/core';
import { SERVER, Resposta } from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class UsuarioService {

	constructor(private http: HttpClient) { }

	getAll(){
		return this.http.get<Resposta>(SERVER+"/usuario")
	}

	delete(id){
		return this.http.delete<Resposta>(SERVER+"/usuario/"+id)
	}

	cadastrar(convite, nome, senha){
		let body= {"codigo_convite":convite, "nome":nome,"senha":senha}
		return this.http.post<Resposta>(SERVER+"/usuario",body)
	}
}
