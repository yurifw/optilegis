import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router"
import { ConviteService } from '../../services/convite.service'
import { UsuarioService } from '../../services/usuario.service'
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-tela-convite',
	templateUrl: './tela-convite.component.html',
	styleUrls: ['./tela-convite.component.css']
})
export class TelaConviteComponent implements OnInit {
	nome = ""
	senha = ""
	senha2 = ""
	conviteInvalido = false
	convite = {email:"",codigo:"",id:null}
	constructor(
		private activatedRoute: ActivatedRoute,
		private conviteService: ConviteService,
		private toastr: ToastrService,
		private usuarioService: UsuarioService,
		private router: Router
	) { }

	ngOnInit(): void {
		let codigo = this.activatedRoute.snapshot.params['codigo'];
		if (codigo){
			this.conviteService.get(codigo).subscribe((res) => {
				if (res.data){
					this.convite = res.data
				} else {
					this.conviteInvalido = true
				}
			})
		} else {
			this.conviteInvalido = true
		}
	}

	cadastrar(){
		if(this.senha != this.senha2){
			this.toastr.warning("As senhas não conferem")
			return
		}
		if(this.nome.length <3){
			this.toastr.warning("Digite seu nome")
			return
		}
		if(this.senha.length < 6){
			this.toastr.warning("A senha deve ter ao menos 6 caracteres")
			return
		}
		this.usuarioService.cadastrar(this.convite.codigo, this.nome, this.senha).subscribe((resp) => {
			if(resp.success){
				this.router.navigate(['login'])
			}
		})
	}

}
