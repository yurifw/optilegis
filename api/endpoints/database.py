import pymysql

MYSQL_HOST = 'localhost'
MYSQL_USER = 'legaluser'
MYSQL_PASSWORD = 'zQqB0nrIDokxEcS14yTD'
MYSQL_DB = 'legaltracker'
MYSQL_PORT = 3306

def ready():
	conn = pymysql.connect(
        host=MYSQL_HOST,
        db=MYSQL_DB,
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        cursorclass=pymysql.cursors.DictCursor
    )
	conn.autocommit(False)
	cursor = conn.cursor()
	return (conn, cursor)
