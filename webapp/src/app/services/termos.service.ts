import { Injectable } from '@angular/core';
import { SERVER, Resposta } from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class TermosService {

	constructor(private http: HttpClient) { }

	insert(termo, emails){
		let body = {"termo":termo, "emails":emails}
		return this.http.post<Resposta>(SERVER+"/termo", body)
	}

	listar(){
		return this.http.get<Resposta>(SERVER+"/termo")
	}

	delete(termoId){
		return this.http.delete<Resposta>(SERVER+"/termo/"+termoId)
	}

	pararAcompanhamentoAdm(termoId, usuarios){
		let body = {"id":termoId, "usuarios":usuarios}
		return this.http.patch<Resposta>(SERVER+"/termo/parar-acompanhamento", body)
	}
	pararAcompanhamentoUser(termoId){
		return this.http.delete<Resposta>(SERVER+"/termo/parar-acompanhamento/"+termoId)
	}
}
