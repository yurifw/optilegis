from bs4 import BeautifulSoup
from lxml import html
import requests
import re
from datetime import date, timedelta
from datetime import datetime
import base64
import re
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os.path import basename
from python_anticaptcha import AnticaptchaClient, ImageToTextTask
import shutil
import codecs
import os
import utils
import sys
import time

script_path = sys.argv[0].replace('run.py','')
captcha_dir = '%scaptchas/' % script_path

def get_page(captcha, data, session):
	# formato data: dd/mm/yyyy
	# http://www.fazenda.rj.gov.br/jrf/c_pauta_jrf.jsp
	payload={
		"acao":"consultar",
		"pagina_erro":"/erro.jsp",
		"pagina_atual":"/c_pauta_jrf.jsp",
		"pagina_retorno":"/r_pauta_jrf.jsp",
		"pagina_retorno_mes":"/r_pauta_mes_jrf.jsp",
		"tpConsulta":"1",
		"edData":data,
		"edPrefProcesso":"",
		"edNumUP":"",
		"edNumProcesso":"",
		"edAnoProcesso":"",
		"edMes":"",
		"edNome":"",
		"cod_img":captcha
	}

	headers = {"Content-Type":"application/x-www-form-urlencoded"}
	time.sleep(1)
	response = session.post("http://www.fazenda.rj.gov.br/jrf/consultaPautaServ", headers = headers, data=payload)

	# with codecs.open("fazendapage.html", 'w','utf8') as page:
	# 	page.write(response.text)
	return response.text

def get_captcha(session):
	# https://github.com/ad-m/python-anticaptcha
	api_key = "b33ea465830a5e1773d191806d590248"
	response = session.get("http://www.fazenda.rj.gov.br/jrf/Kaptcha.jpg", stream=True)
	response.raw.decode_content = True
	img = response.raw
	with open('%scaptcha.jpg' % captcha_dir, 'wb') as f:
		shutil.copyfileobj(img, f)

	resposta = ''
	script_path = sys.argv[0].replace('run.py','')
	with open('%scaptcha.jpg' % captcha_dir, 'rb') as captcha_fp:
		tentativas = 0
		while len(resposta) < 1 and tentativas < 5:
			try:
				client = AnticaptchaClient(api_key)
				task = ImageToTextTask(captcha_fp)
				job = client.createTask(task)
				job.join()
				resposta = job.get_captcha_text()
				print(">>> captcha: %s" % resposta)
			except Exception as e:
				utils.log("captcha falhou: %s" % str(e))
				tentativas = tentativas + 1
	os.rename('%scaptcha.jpg' % captcha_dir, '%s%s.jpg' % (captcha_dir, resposta))
	return resposta

def parse_pauta(pauta_text):
	pauta = {}

	result = re.search("([0-9]{1,}).*Turma", pauta_text)
	pauta['turma'] = None if result is None else result.group(1).strip()
	# print(pauta_text.decode('utf-8','ignore').encode("utf-8"))
	# print(pauta)
	# print("=====")

	result = re.search("Processo\sN.\s(.*)", pauta_text)
	pauta['num_processo'] = None if result is None else result.group(1).strip()

	result = re.search("Autuada:\s(.*)", pauta_text)
	pauta['autuada'] = None if result is None else result.group(1).strip()

	result = re.search("vel:\s(.*)", pauta_text)
	pauta['responsavel'] = None if result is None else result.group(1).strip()

	result = re.search("Relator:\s(.*)", pauta_text)
	pauta['relator'] = None if result is None else result.group(1).strip()

	result = re.search("do dia\s(.*?)\s", pauta_text)
	dia = None if result is None else result.group(1).strip()
	result = re.search("s\s([0-9]{2}:[0-9]{2}:[0-9]{2})\.", pauta_text)
	hora = None if result is None else result.group(1).strip()
	pauta['data'] ="%s %s" % (dia, hora)

	result = re.search("Virtual:\s(.*)", pauta_text)
	pauta['sessao_link'] = None if result is None else result.group(1).strip()

	result = re.search("mero da Sessão:\s(.*)", pauta_text)
	pauta['sessao_num'] = None if result is None else result.group(1).strip()

	result = re.search("Senha da Sessão:\s(.*)", pauta_text)
	pauta['sessao_senha'] = None if result is None else result.group(1).strip()

	result = re.search("Status da Sessão:\s(.*)", pauta_text)
	pauta['sessao_status'] = None if result is None else result.group(1).strip()

	return pauta

def insert_pauta(pauta, cursor):
	try:
		sql = """insert into pauta (turma, num_processo, autuada, responsavel, relator, data, sessao_link, sessao_num, sessao_senha, sessao_status)
		values (%s, %s, %s, %s, %s, str_to_date(%s,'%%d/%%m/%%Y %%H:%%i:%%S'), %s, %s, %s, %s)"""
		data = [
			pauta['turma'],
			pauta['num_processo'],
			pauta['autuada'],
			pauta['responsavel'],
			pauta['relator'],
			pauta['data'],
			pauta['sessao_link'],
			pauta['sessao_num'],
			pauta['sessao_senha'],
			pauta['sessao_status']
		]
		cursor.execute(sql, data)
	except:
		pass

def pauta_contains_termos(pauta, termos):
	for key in pauta:
		for termo in termos:
			termo = termo.upper()
			if termo in str(pauta[key]).upper():
				return True
	return False


def get_pautas(cursor, data):
	session = requests.Session()
	pautas = []
	captcha = get_captcha(session)
	page = get_page(captcha, data, session)
	tree = html.fromstring(page)
	cards = tree.xpath("//font[@size='2' and contains(@face, 'sans')]")
	for card in cards:
		card_text = card.text_content()
		pauta = parse_pauta(str(card_text))
		insert_pauta(pauta, cursor)
		pautas.append(pauta)

	return pautas
