import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { ToastrService } from 'ngx-toastr';
import { PagamentoService } from '../../services/pagamento.service'
import Swal from 'sweetalert2'	//https://sweetalert2.github.io/	https://stackoverflow.com/questions/41684114/angular-2-easy-way-to-make-a-confirmation-dialog

@Component({
	selector: 'app-tela-pagamento',
	templateUrl: './tela-pagamento.component.html',
	styleUrls: ['./tela-pagamento.component.css']
})
export class TelaPagamentoComponent implements OnInit {
	statusPagamento = {
		"notificar_falta_pagamento":false,
		"pagamento_realizado":false,
		"subscription_id": null,
		"testando": false
	}

	subscription = {
		"card_number":"",
		"card_cvv": "",
		"card_holder_name":"",
		"card_expiration_date":"",
		"payment_method": "credit_card",
		"plan_id": "",
		"customer": {
			"email":"",
			"document_number":"",
			"name":""
		}
	}

	constructor(
		private authService: AuthService,
		private toastr: ToastrService,
		private pagamentoService: PagamentoService
	) { }

	ngOnInit(): void {
		this.authService.verifyToken().subscribe(resp => {
			let escritorio = resp.data.payload.escritorio_id
			this.pagamentoService.getStatus(escritorio).subscribe(resp =>{
				this.statusPagamento = resp.data
				console.log(resp)
			})
		})
	}

	confirmar(){
		if(this.statusPagamento.pagamento_realizado && this.statusPagamento.subscription_id){
			this.pagamentoService.alterarCartao(this.subscription).subscribe(resp =>{
				console.log(resp)
			})
		} else {
			this.pagamentoService.criarAssinatura(this.subscription).subscribe(resp =>{
				console.log(resp)
			})
		}
	}

	cancelar(){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'Seu escritório não receberá mais nenhum resumo diário',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}).then((result) => {
			if (result.value) {
				this.pagamentoService.cancelarAssinatura().subscribe(resp =>{
					console.log(resp)
				})
			}
		})


	}

}
