from flask import Flask, make_response
from flask import request
import json
import re
from datetime import datetime, timedelta
import pagarme  #https://github.com/pagarme/pagarme-python
from . import database as db
from .auth import decode_token, requires_admin

test_plan_id = '510934'
live_plan_id = '1089470'
live_key = "ak_live_UEwUmhBj2llQ3c12WGpHrXTWdcOvsW"
test_key = "ak_test_Hq2RjHputSDexBrJnrdZdC6XVrk1WY"
DIAS_GRATIS = 14

def _get_environment_variable(host):
	if 'admdo.com.br' in host:
		return (live_key, live_plan_id)
	else:
		return (test_key, test_plan_id)

def _remove_masks(text):
	return re.sub("[^0-9]", "", text)

@requires_admin(True)
def criar_assinatura():
	conn, cursor = db.ready()
	try:
		key, plan_id = _get_environment_variable(request.headers['host'])
		escritorio_id = decode_token(request.headers['Authorization'])['payload']['escritorio_id']
		pagarme.authentication_key(key)
		s = request.json
		s['plan_id'] = plan_id
		s['card_number'] = _remove_masks(s['card_number'])
		s['card_cvv'] = _remove_masks(s['card_cvv'])
		s['card_expiration_date'] = _remove_masks(s['card_expiration_date'])
		s['customer']['document_number'] = _remove_masks(s['customer']['document_number'])
		subscription = pagarme.subscription.create(s)
		sql = "update escritorio set subscription_id = %s where id = %s"
		data = [subscription['id'], escritorio_id]
		cursor.execute(sql, data)
		conn.commit()
		return make_response({"msg": "Pagamento Realizado com sucesso!","data":None, "success":True})

	except Exception as e:

		erro = str(e).replace("'",'"').replace("None","null")
		erro = json.loads(erro)
		if len(erro) > 0 and 'message' in erro[0]:
			return make_response({"msg":"%s (%s)" % (erro[0]['message'], erro[0]['parameter_name']), "success":False, "data":None})
		return make_response({"msg":str(e), "success":False, "data":None})

	finally:
		cursor.close()
		conn.close()

@requires_admin(True)
def cancelar_assinatura():
	conn, cursor = db.ready()
	try:
		key, plan_id = _get_environment_variable(request.headers['host'])
		escritorio_id = decode_token(request.headers['Authorization'])['payload']['escritorio_id']
		cursor.execute("select * from escritorio where id = %s", [escritorio_id])
		e = cursor.fetchone()
		pagarme.authentication_key(key)
		subscription = pagarme.subscription.cancel(e['subscription_id'])
		sql = "update escritorio set subscription_id = null where id = %s"
		data = [escritorio_id]
		cursor.execute(sql, data)
		conn.commit()
		return make_response({"msg": "Pagamento cancelado com sucesso","data":None, "success":True})

	except Exception as e:
		erro = json.loads(str(e).replace("'",'"'))
		if len(erro) > 0 and 'message' in erro[0]:
			return make_response({"msg":"%s (%s)" % (erro[0]['message'], erro[0]['parameter_name']), "success":False, "data":None})
		return make_response({"msg":str(e), "success":False, "data":None})

@requires_admin(True)
def trocar_cartao():
	conn, cursor = db.ready()
	try:
		key, plan_id = _get_environment_variable(request.headers['host'])
		escritorio_id = decode_token(request.headers['Authorization'])['payload']['escritorio_id']
		cursor.execute("select * from escritorio where id = %s", [escritorio_id])
		e = cursor.fetchone()
		pagarme.authentication_key(key)
		s = request.json
		s['plan_id'] = plan_id
		s['payment_method'] = 'credit_card'
		s['card_number'] = _remove_masks(s['card_number'])
		s['card_cvv'] = _remove_masks(s['card_cvv'])
		s['card_expiration_date'] = _remove_masks(s['card_expiration_date'])
		s['customer']['document_number'] = _remove_masks(s['customer']['document_number'])

		subscription = pagarme.subscription.update(e['subscription_id'], s)

		sql = "update escritorio set subscription_id = %s where id = %s"
		data = [subscription['id'], escritorio_id]
		cursor.execute(sql, data)
		conn.commit()
		return make_response({"msg": "Cartão alterado com sucesso!","data":None, "success":True})

	except Exception as e:
		erro = json.loads(str(e).replace("'",'"'))
		if len(erro) > 0 and 'message' in erro[0]:
			return make_response({"msg":"%s (%s)" % (erro[0]['message'], erro[0]['parameter_name']), "success":False, "data":None})
		return make_response({"msg":str(e), "success":False, "data":None})

	finally:
		cursor.close()
		conn.close()

def get_status(escritorio_id):
	conn, cursor = db.ready()
	try:

		key, plan_id = _get_environment_variable(request.headers['host'])
		status = {"pagamento_realizado":None, "notificar_falta_pagamento": None,"testando":False}

		cursor.execute("select * from escritorio where id = %s",[escritorio_id])
		escritorio = cursor.fetchone()
		status['notificar_falta_pagamento'] = escritorio['notificar_falta_pagamento'] == 1
		status['subscription_id'] = escritorio['subscription_id']



		subscription_id = escritorio['subscription_id']
		if subscription_id is None:
			status['pagamento_realizado'] = False
		else:
			pagarme.authentication_key(key)
			subscription = pagarme.subscription.find_by({"id": subscription_id})[0]
			if subscription['status'] == 'paid':
				status['pagamento_realizado'] = True
			else:
				status['pagamento_realizado'] = False

		if escritorio['data_cadastro'] + timedelta(days=DIAS_GRATIS) > datetime.now():
			status['pagamento_realizado'] = True

		return make_response({"msg": None,"data":status, "success":True})
	finally:
		cursor.close()
		conn.close()

@requires_admin(True)
def notificar_falta_pagamento():
	conn, cursor = db.ready()
	try:
		token = decode_token(request.headers['Authorization'])['payload']
		escritorio_id = token['escritorio_id']
		notificar = request.json['notificar']
		cursor.execute("update escritorio set notificar_falta_pagamento = %s where id = %s",[notificar, escritorio_id])
		conn.commit()
		msg = ""
		if notificar:
			msg = "Você receberá um email se o pagamento não for realizado."
		else:
			msg = "Você não receberá mais emails avisando sobre o pagamento."
		return make_response({"msg": msg,"data":None, "success":True})
	finally:
		cursor.close()
		conn.close()
