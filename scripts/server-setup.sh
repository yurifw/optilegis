apt update
apt install -y git
apt install -y vim
apt install -y curl
apt install -y zip
apt install -y nodejs
curl -L https://npmjs.org/install.sh | sh
npm install -g @angular/cli
apt -y install mariadb-server
apt -y install mariadb-client
apt install -y default-libmysqlclient-dev
apt install -y python3-pip
apt install -y python-certbot-nginx
pip3 install wheel
pip3 install flask
pip3 install flask_cors
pip3 install pymysql
pip3 install PyJWT
pip3 install bcrypt
pip3 install --upgrade pip
pip3 install PyMuPDF
pip3 install BeautifulSoup4
pip3 install lxml
pip3 install python-anticaptcha
apt -y install build-essential libssl-dev libffi-dev python3-dev
apt -y install nginx
pip3 install gunicorn
echo "[mariadb]" >> /etc/my.cnf
echo "default_time_zone = -3:00" >> /etc/my.cnf
echo "[mysqld]" >> /etc/my.cnf
echo "innodb_log_file_size=500M" >> /etc/my.cnf
echo "max_allowed_packet=50M" >> /etc/my.cnf
echo "wait_timeout = 600" >> /etc/my.cnf
echo "[client]" >> /etc/my.cnf
echo "max_allowed_packet=50M" >> /etc/my.cnf
rm /var/lib/mysql/ib_logfile*
sed -i 's-max_allowed_packet\s*=\s*16M-max_allowed_packet      = 50M-g' /etc/mysql/mariadb.conf.d/50-server.cnf
/etc/init.d/mysql restart
mysql_secure_installation
dpkg-reconfigure tzdata

mkdir /root/legaltracker/bots/captchas
chmod +x /root/legaltracker/bots/run.py
echo "30 9 * * 1,2,3,4,5 python3 /root/legaltracker/bots/run.py >> /root/legaltracker/bot.log" >> mycron
crontab mycron
rm mycron

certbot --nginx -d admdo.com.br -d www.admdo.com.br
certbot renew --dry-run
