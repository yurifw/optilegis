import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaTermosComponent } from './tela-termos.component';

describe('TelaTermosComponent', () => {
  let component: TelaTermosComponent;
  let fixture: ComponentFixture<TelaTermosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaTermosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaTermosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
