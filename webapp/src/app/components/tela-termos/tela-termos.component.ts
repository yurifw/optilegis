import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'


@Component({
	selector: 'app-tela-termos',
	templateUrl: './tela-termos.component.html',
	styleUrls: ['./tela-termos.component.css']
})
export class TelaTermosComponent implements OnInit {

	isAdmin = false
	constructor(private authService: AuthService) { }

	ngOnInit(): void {
		this.authService.verifyToken().subscribe((resp) => {
			if(resp.success && resp.data.isValid){
				this.isAdmin = resp.data.payload.admin
			}
		})
	}

}
