import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaConviteComponent } from './tela-convite.component';

describe('TelaConviteComponent', () => {
  let component: TelaConviteComponent;
  let fixture: ComponentFixture<TelaConviteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaConviteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaConviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
