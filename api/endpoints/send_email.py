import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

TEMPLATE = {
	"convite":"""
	<html>
		<body>
			<div style='background-image: linear-gradient(to top right, #54F2F2, #5EB1BF); width:500px; margin: 0 auto; padding:50px;'>
				<div style="min-height:200px; margin: 0 auto; border-radius: 30px; background-color:#ffffff; opacity:0.7; padding:15px;text-align: justify;">
					<h4>Olá</h4>
					para se cadastrar no Adm DO basta acessar este link:
						<a href="https://admdo.com.br/convite/{codigo}">https://admdo.com.br/convite/{codigo}</a>
				</div>
			</div>
		</body>
	</html>

		""",
		"esqueci-senha":"""
		<html>
			<body>
				<div style='background-image: linear-gradient(to top right, #54F2F2, #5EB1BF); width:500px; margin: 0 auto; padding:50px;'>
					<div style="min-height:200px; margin: 0 auto; border-radius: 30px; background-color:#ffffff; opacity:0.7; padding:15px;text-align: justify;">
						<h4>Olá</h4>
						Você está recebendo este email pois esqueceu sua senha. Estamos enviando um link com validade de 15 minutos, ao clicar neste link você será autenticado e terá 15 minutos para trocar sua senha.
						Para se autenticar no Adm DO basta <a href="https://admdo.com.br/login/{codigo}">clicar aqui</a>
					</div>
				</div>
			</body>
		</html>
		"""
}

def send_email(para, assunto, corpo, anexos=None):
	"""
		para: list - list com enderecos de email que serao os destinatarios
		assunto: str - aparecera como assunto
		corpo: str - texto que sera o corpo do email
		anexos: list - lista de caminhos de arquivos para serem anexados no email
	"""
	# CONFIG #
	email_server = "mail.admdo.com.br"
	email_port = "465"
	email_user = "noreply@admdo.com.br"
	email_pwd = "HUQY@05nlLY"
	####################################

	msg = MIMEMultipart()
	msg['From'] = email_user
	msg['To'] = ','.join(para)
	msg['Subject'] = assunto

	msg.attach(MIMEText(corpo, 'html'))

	for f in anexos or []:
		with open(f, "rb") as fil:
			part = MIMEApplication(
				fil.read(),
				Name=basename(f)
			)
		# After the file is closed
		part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
		msg.attach(part)
	smtp = smtplib.SMTP_SSL(email_server, email_port)
	smtp.login(email_user, email_pwd)
	smtp.sendmail(email_user, para, msg.as_string())
	smtp.quit()
