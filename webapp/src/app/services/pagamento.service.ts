import { Injectable } from '@angular/core';
import { SERVER, Resposta } from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class PagamentoService {

	constructor(private http: HttpClient) { }

	getStatus(escritorioId){
		return this.http.get<Resposta>(SERVER+"/pagamento/status/"+escritorioId)
	}

	notificarFaltaPagamento(notificar){
		return this.http.patch<Resposta>(SERVER+"/pagamento/notificacao", {"notificar":notificar})
	}

	criarAssinatura(assinatura){
		return this.http.post<Resposta>(SERVER+"/pagamento/assinatura", assinatura)
	}

	cancelarAssinatura(){
		return this.http.delete<Resposta>(SERVER+"/pagamento/assinatura")
	}

	alterarCartao(assinatura){
		return this.http.patch<Resposta>(SERVER+"/pagamento/assinatura", assinatura)
	}

}
