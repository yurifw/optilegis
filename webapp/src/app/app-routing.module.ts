import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TelaLoginComponent } from './components/tela-login/tela-login.component'
import { TelaTermosComponent } from './components/tela-termos/tela-termos.component'
import { TelaUsuariosComponent } from './components/tela-usuarios/tela-usuarios.component'
import { TelaConviteComponent } from './components/tela-convite/tela-convite.component'
import { TelaConfigComponent } from './components/tela-config/tela-config.component'
import { TelaPagamentoComponent } from './components/tela-pagamento/tela-pagamento.component';


const routes: Routes = [
	{ path: 'termos', component: TelaTermosComponent },
	{ path: 'usuarios', component: TelaUsuariosComponent },
	{ path: '', component: TelaLoginComponent},
	{ path: 'login', component: TelaLoginComponent},
	{ path: 'login/:jwt', component: TelaLoginComponent},
	{ path: 'convite/:codigo', component: TelaConviteComponent},
	{ path: 'config', component: TelaConfigComponent},
	{ path: 'pagamento', component: TelaPagamentoComponent},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
