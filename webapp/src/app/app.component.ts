import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service'
import { Router } from "@angular/router"

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	title = 'LegalTracker';
	isLoggedIn = false;
	isAdmin = false;
	nome = ""

	constructor(private auth: AuthService, private router: Router) { }

	ngOnInit(): void {
		this.auth.loggedIn.subscribe((isLoggedIn)=>{

			this.isLoggedIn = isLoggedIn
			if(isLoggedIn){
				this.updateUser()
			}
		})

		this.updateUser()
	}

	updateUser(){
		this.auth.verifyToken().subscribe((resp)=>{
			this.isLoggedIn = resp.data.isValid;
			this.nome = resp.data.payload.nome
			this.isAdmin = resp.data.payload.admin
		})
	}

	logout(){
		localStorage.removeItem('jwt')
		this.isLoggedIn = false;
		this.auth.loggedIn.emit(false)
		this.router.navigate([''])
	}

	navigate(route){
		this.router.navigate([route])
	}

}
