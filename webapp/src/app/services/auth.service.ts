import { Injectable, Output } from '@angular/core';
import { SERVER, Resposta } from "../commons"
import { HttpClient } from '@angular/common/http';
import { EventEmitter } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class AuthService {
	@Output() loggedIn: EventEmitter<any> = new EventEmitter();

	constructor(private http: HttpClient) { }

	getToken(email:string, password:string){
		let body = {"email":email, "senha":password}
		return this.http.post<Resposta>(SERVER+"/login", body)

	}

	verifyToken(){
		return this.http.get<Resposta>(SERVER+"/login/verify")
	}

	resetarSenha(email){
		return this.http.patch<Resposta>(SERVER+"/login/esqueci-senha", {"email":email} )
	}

	trocarSenha(novaSenha){
		return this.http.patch<Resposta>(SERVER+"/login", {"senha":novaSenha} )
	}
}
