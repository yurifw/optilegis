import utils
import diario_oficial_rj
import scrape_fazenda
from datetime import datetime, timedelta
from time import sleep
import sys
import requests
import json
import traceback

conn = None
cursor = None
dias_pauta = 10
script_path = sys.argv[0].replace('run.py','')

msg = """
	<html>
		<body>

			<div style='background-image: linear-gradient(to top right, #54F2F2, #5EB1BF); width:500px; margin: 0 auto; padding:15px;'>
				<div style="min-height:200px; margin: 0 auto; border-radius: 30px; background-color:#ffffff; opacity:0.7; padding:15px;text-align: justify;">
					<h4>Bom dia</h4>
					Este é o seu resumo diário. Estamos enviando também uma versão personalizada do Diário Oficial com os termos que você acompanha todos destacados.<br>
					<span style="text-decoration:underline">Termos encontrados no diário oficial: </span>
					<div> {recorte_result} </div>
					<span style="text-decoration:underline">Termos encontrados nas pautas</span>:
					<div> {pautas_result} </div>
					<span style="font-size: 50%"> Enviado {hora_envio}</span>
				</div>

			</div>
		</body>
	</html>
"""

try:
	report = {
		"name": "AdmDO (bots)",
		"time_beginning": datetime.today().strftime("%d/%m/%Y %H:%M:%S"),
		"time_ending": None,
		"run_successfull": True,
		"report_data": {
			"download_do": False,
			"pautas_encontradas": 0,
			"emails_enviados": 0
		}
	}

	utils.log("iniciando")
	conn, cursor = utils.connect_db()
	termos = utils.get_termo_list(cursor)
	utils.log("lista de termos carregada")

	utils.log("baixando diario oficial")
	diario_oficial_rj.download_pdf()
	diario_oficial_rj.salva_do(cursor)
	utils.log("diario oficial baixado e salvo no banco")
	report['report_data']['download_do'] = True

	utils.log("baixando pautas")
	pautas = []
	hoje = datetime.now()
	for i in range(dias_pauta):
		data = hoje.strftime("%d/%m/%Y")
		p = scrape_fazenda.get_pautas(cursor, data)
		pautas.extend(p)
		hoje = hoje + timedelta(days = 1)
	utils.log("pautas baixadas e salvas no banco")
	report['report_data']['pautas_encontradas'] = len(pautas)

	utils.log("percorrendo lista de emails e termos")
	for email in termos:
		mail_pauta = ''
		for pauta in pautas:
			if not scrape_fazenda.pauta_contains_termos(pauta, termos[email]):
				continue
			mail_pauta = "%s<b>%sª Turma - Processo Nº %s</b><br>" % (mail_pauta, pauta['turma'], pauta['num_processo'])
			mail_pauta = "%sAutuada: %s<br>" % (mail_pauta, pauta['autuada'])
			mail_pauta = "%sRepartição Responável: %s<br>" % (mail_pauta, pauta['responsavel'])
			mail_pauta = "%sRelator: %s<br>" % (mail_pauta, pauta['relator'])
			mail_pauta = "%sPauta de Julgamento para Sessão Ordinária do dia : %s<br>" % (mail_pauta, pauta['data'])
			mail_pauta = "%sLink da Sessão Virtual: <a href='%s' target='_blank'>%s</a><br>" % (mail_pauta, pauta['sessao_link'], pauta['sessao_link'])
			mail_pauta = "%sNúmero da Sessão: %s<br>" % (mail_pauta, pauta['sessao_num'])
			mail_pauta = "%sSenha da Sessão: %s<br>" % (mail_pauta, pauta['sessao_senha'])
			mail_pauta = "%sStatus da Sessão: %s<br>" % (mail_pauta, pauta['sessao_status'])
			mail_pauta = "%s%s" % (mail_pauta, "<hr>")


		resultado_do = diario_oficial_rj.search_pdf(termos[email])

		mail_do = "<ul>"
		for termo in resultado_do:
			if len(resultado_do[termo]) == 0:
				continue
			mail_do = "%s<li><b>%s</b>%s" %(mail_do, termo, "  encontrado nas páginas: <br>")
			for pagina in resultado_do[termo]:
				mail_do = "%s%s, " %(mail_do, pagina)
			mail_do = "%s%s" % (mail_do[:-2], "</li>")
		mail_do = "%s%s" %(mail_do, "</ul>")

		utils.log("enviando email para %s" % email)
		sleep(8)
		envio = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
		body = msg.format(recorte_result = mail_do, pautas_result = mail_pauta, hora_envio = envio)
		utils.envia_mail([email], 'Resumo Adm DO', body, ['%sdodestacado.pdf' % script_path])
		utils.log("email enviado")
		report['report_data']['emails_enviados'] = report['report_data']['emails_enviados'] +1
except Exception as e:
	traceback.print_exc()
	report['run_successfull'] = False
finally:
	cursor.close()
	conn.close()

	project_key = 'nBho2TBP5kvZgRqEyBc6'
	report["time_ending"] = datetime.today().strftime("%d/%m/%Y %H:%M:%S")
	url = "https://polarium.com.br/api/reports/%s" % project_key
	headers = {
		'Authorization': project_key,
		'Content-Type': 'application/json'
	}
	r = requests.post(url, headers=headers, data = json.dumps(report))

	# diario_oficial_rj.limpa_pdfs()
	print("=========================================")
