import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { ToastrService } from 'ngx-toastr';
import { PagamentoService } from '../../services/pagamento.service'

@Component({
	selector: 'app-tela-config',
	templateUrl: './tela-config.component.html',
	styleUrls: ['./tela-config.component.css']
})
export class TelaConfigComponent implements OnInit {
	novaSenha = ""
	novaSenha2 = ""
	statusPagamento = {
		"notificar_falta_pagamento":false,
		"pagamento_realizado":false,
		"subscription_id": null
	}

	constructor(
		private authService: AuthService,
		private toastr: ToastrService,
		private pagamentoService: PagamentoService
	) { }

	ngOnInit(): void {
		this.authService.verifyToken().subscribe(resp => {
			let escritorio = resp.data.payload.escritorio_id
			this.pagamentoService.getStatus(escritorio).subscribe(resp =>{
				this.statusPagamento = resp.data
				console.log(resp)
			})
		})
	}

	alterarNotificacaoPagamento(notificar){
		this.pagamentoService.notificarFaltaPagamento(notificar).subscribe()
	}

	alterar(){
		if(this.novaSenha.length < 6){
			this.toastr.warning("A senha deve ter pelo menos 6 caracteres")
			return
		}
		if(this.novaSenha !== this.novaSenha2){
			this.toastr.warning("As senhas não conferem")
			return
		}
		this.authService.trocarSenha(this.novaSenha).subscribe((resp) =>{
			if(resp.success){
				this.novaSenha = ""
				this.novaSenha2 = ""
			}
		})
	}

}
